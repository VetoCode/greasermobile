﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace GreaserApi.Tests.Hubs
{
    public class ChatHubShould
    {
        [Fact]
        public void ChatHub_NoInput_ReturnTrue()
        {
            Assert.True(true, "ChatHub with no input should return true.");
        }
    }
}
