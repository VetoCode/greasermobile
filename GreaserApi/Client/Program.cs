﻿using IdentityModel.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Client
{
    public class Program
    {
        private static async Task Main()
        {
            Console.WriteLine("-----Program Start-----");
            // discover endpoints from metadata
            var client = new HttpClient();

            var disco = await client.GetDiscoveryDocumentAsync("https://localhost:5001");
            if (disco.IsError)
            {
                Console.WriteLine("-------Discovery Error---------");
                Console.WriteLine(disco.Error);
                return;
            }

            //request token
            var tokenResponse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = "client",
                ClientSecret = "secret",

                Scope = "GreaserApi"
            });

            if (tokenResponse.IsError)
            {
                Console.WriteLine("--------tokenResponse Error-----------");
                Console.WriteLine(tokenResponse.Error);
                return;
            }

            Console.WriteLine(tokenResponse.Json);
            Console.WriteLine("\n\n");

            // call api
            var apiClient = new HttpClient();
            apiClient.SetBearerToken(tokenResponse.AccessToken);

            var response = await apiClient.GetAsync("https://localhost:6001/identity");
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine(response.StatusCode);
            }
            else
            {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine(JArray.Parse(content));
            }

            Console.WriteLine();
            Console.WriteLine("--------NEXT TEST----------");
            Console.WriteLine("--------Beginning Mobile----------");
            Console.WriteLine();


            var client2 = new HttpClient();
            var passwordTokenResponse = await client2.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = "GreaserMobileDevice",
                ClientSecret = "Gimobile",
                Scope = "GreaserApi",
                
                UserName = "chsakell",
                Password = "$AspNetIdentity10$"
            });

            if (passwordTokenResponse.IsError)
            {
                Console.WriteLine("Received Password Token Error");
                Console.WriteLine("Error: " + passwordTokenResponse.Error);
                Console.WriteLine("Error description: " + passwordTokenResponse.ErrorDescription);
                Console.WriteLine("Exception: " + passwordTokenResponse.Exception);
                Console.WriteLine("Raw: " + passwordTokenResponse.Raw);
                return;
            }

            Console.WriteLine(passwordTokenResponse.Json);
            Console.WriteLine("\n\n");

            // call api
            var apiClient2 = new HttpClient();
            apiClient2.SetBearerToken(passwordTokenResponse.AccessToken);

            var response2 = await apiClient2.GetAsync("https://localhost:6001/identity");
            if (!response2.IsSuccessStatusCode)
            {
                Console.WriteLine(response2.StatusCode);
            }
            else
            {
                var content2 = await response2.Content.ReadAsStringAsync();
                Console.WriteLine(JArray.Parse(content2));
            }

        }

        private static void TestMobileClient(){
            
            var client = new HttpClient();
            var disco = client.GetDiscoveryDocumentAsync("https://localhost:5001").Result;
            var client2 = new HttpClient();
            var passwordTokenResponse = client2.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = disco.TokenEndpoint,
                ClientId = "GreaserMobileDevice",
                ClientSecret = "Gimobile",
                Scope = "GreaserApi",
                
                UserName = "chsakell",
                Password = "$AspNetIdentity10$"
            }).Result;

            if (passwordTokenResponse.IsError)
            {
                Console.WriteLine(passwordTokenResponse.Error);
                return;
            }
        }

        private static void ProcessResponse(TokenResponse tokenResponse){
            if(tokenResponse.IsError)
                HandleTokenResponseError(tokenResponse.Error);
        }

        private static void HandleTokenResponseError(string errorContent){

        }

        private async static void HandleHttpResponseError(HttpContent errorContent){
            var httpContentAsString = await errorContent.ReadAsStringAsync();
            Console.WriteLine(JArray.Parse(httpContentAsString));
        }
    }
}
