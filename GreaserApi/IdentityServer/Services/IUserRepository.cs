﻿using IdentityServer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Services
{
    public interface IUserRepository
    {
        bool ValidateCredentials(string username, string password);

        Task<ApplicationUser> FindBySubjectId(string subjectId);

        ApplicationUser FindByUsername(string username);
    }
}
