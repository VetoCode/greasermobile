﻿using IdentityServer4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer
{
    public interface IConfig
    {
        public string BaseUrl { get; set; }
        public string ApiBaseUrl { get; set; }
        public string DbConnectionString { get; set; }
        public IEnumerable<IdentityResource> IdentityResources { get; set; }
        public IEnumerable<ApiScope> ApiScopes { get; set; }
        public IEnumerable<Client> Clients { get; set; }
    }
}
