﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityServer4;
using IdentityServer4.Models;
using System.Collections.Generic;

namespace IdentityServer
{
    public static class Config
    {
        public static string CurrentUrl = Constants.LocalBaseUrl;
        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
                {
                    new Client
                    {
                        ClientId = "client",
                        AllowedGrantTypes = GrantTypes.ClientCredentials,
                        ClientSecrets =
                        {
                            new Secret("secret".Sha256())
                        },
                        AllowedScopes = { "GreaserApi" }
                    },
                    // interactive ASP.NET Core MVC client
                    new Client
                    {
                        ClientId = "GreaserMvc",
                        ClientSecrets = { new Secret("GreaserSecret".Sha256()) },

                        AllowedGrantTypes = GrantTypes.Code,
                        RequirePkce = true,
                        RedirectUris = { CurrentUrl },
                        PostLogoutRedirectUris = { CurrentUrl },
                        AllowedCorsOrigins = { CurrentUrl },

                        AllowOfflineAccess = true,

                        AllowedScopes = new List<string>
                        {
                            IdentityServerConstants.StandardScopes.OpenId,
                            IdentityServerConstants.StandardScopes.Profile,
                            "GreaserApi"
                        }
                    },
                    // Mobile endpoint with limited access
                    new Client
                    {
                        ClientId = "GreaserMobileDevice",
                        ClientSecrets = { new Secret("Gimobile".Sha256()) },
                        RequirePkce = true,
                        RequireConsent = false,
                        AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials,
                        AllowOfflineAccess = true,

                        RedirectUris = { CurrentUrl },
                        PostLogoutRedirectUris = { CurrentUrl },
                        AllowedCorsOrigins = { CurrentUrl },
                        AccessTokenLifetime = 40,
                        
                        AllowedScopes = new List<string>
                        {
                            IdentityServerConstants.StandardScopes.OpenId,
                            IdentityServerConstants.StandardScopes.Profile,
                            IdentityServerConstants.StandardScopes.Email,
                            IdentityServerConstants.StandardScopes.OfflineAccess,
                            "GreaserApi"
                        }
                    }
                };
        }

        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
                {
                    new IdentityResources.OpenId(),
                    new IdentityResources.Profile(),
                }; ;
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("GreaserApi", "Greaser API"),
                new ApiResource("GreaserMobileDevice", "Greaser Xamarin Clients")
            };
        }

        public static IEnumerable<ApiScope> GetApiScopes()
        {
            return new List<ApiScope>
            {
                new ApiScope("GreaserApi", "Greaser API")
            };
        }
    }
}