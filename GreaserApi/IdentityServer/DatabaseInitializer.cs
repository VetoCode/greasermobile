﻿using IdentityModel;
using IdentityServer.Data;
using IdentityServer.Models;
using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentityServer
{
    public class DatabaseInitializer
    {
        public IConfiguration Configuration { get; }

        public DatabaseInitializer(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public static void Init(IServiceProvider provider)
        {
            AddTestUsers(provider);

            var userManager = provider.GetRequiredService<UserManager<ApplicationUser>>();
            var vetoApplicationUser = userManager.FindByNameAsync("contagious.thoughts@gmail.com").Result;
            if (vetoApplicationUser == null)
            {
                vetoApplicationUser = new ApplicationUser
                {
                    UserName = "contagious.thoughts@gmail.com"
                };
                var result = userManager.CreateAsync(vetoApplicationUser, "##TrainingDay01").Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                vetoApplicationUser = userManager.FindByNameAsync("contagious.thoughts@gmail.com").Result;

                result = userManager.AddClaimsAsync(vetoApplicationUser, new Claim[]{
                    new Claim(JwtClaimTypes.Name, "Veto Cohen"),
                    new Claim(JwtClaimTypes.GivenName, "Veto"),
                    new Claim(JwtClaimTypes.FamilyName, "Cohen"),
                    new Claim(JwtClaimTypes.Email, "contagious.thoughts@gmail.com"),
                    new Claim(JwtClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean),
                    new Claim(JwtClaimTypes.WebSite, "https://google.com"),
                    new Claim(JwtClaimTypes.Address, @"{ 'street_address': 'localhost 10', 'postal_code': 11146, 'country': 'Greece' }",
                        IdentityServer4.IdentityServerConstants.ClaimValueTypes.Json)
                }).Result;

                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }
                Console.WriteLine("contagious.thoughts@gmail.com created");
            }
            else
            {
                Console.WriteLine("contagious.thoughts@gmail.com already exists");
                var result = userManager.AddClaimsAsync(vetoApplicationUser, new Claim[]{
                    new Claim(JwtClaimTypes.Name, "Veto Cohen"),
                    new Claim(JwtClaimTypes.GivenName, "Veto"),
                    new Claim(JwtClaimTypes.FamilyName, "Cohen"),
                    new Claim(JwtClaimTypes.Email, "contagious.thoughts@gmail.com"),
                    new Claim(JwtClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean),
                    new Claim(JwtClaimTypes.WebSite, "https://google.com"),
                    new Claim(JwtClaimTypes.Address, @"{ 'street_address': 'localhost 10', 'postal_code': 11146, 'country': 'Greece' }",
                        IdentityServer4.IdentityServerConstants.ClaimValueTypes.Json)
                }).Result;
            }
        }

        private static void AddTestUsers(IServiceProvider provider)
        {
            var userManager = provider.GetRequiredService<UserManager<ApplicationUser>>();

#region chsakell
            var chsakell = userManager.FindByNameAsync("chsakell").Result;
            if (chsakell == null)
            {
                chsakell = new ApplicationUser
                {
                    UserName = "chsakell"
                };
                var result = userManager.CreateAsync(chsakell, "$AspNetIdentity10$").Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                chsakell = userManager.FindByNameAsync("chsakell").Result;

                result = userManager.AddClaimsAsync(chsakell, new Claim[]{
                    new Claim(JwtClaimTypes.Name, "Chris Sakellarios"),
                    new Claim(JwtClaimTypes.GivenName, "Christos"),
                    new Claim(JwtClaimTypes.FamilyName, "Sakellarios"),
                    new Claim(JwtClaimTypes.Email, "chsakellsblog@blog.com"),
                    new Claim(JwtClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean),
                    new Claim(JwtClaimTypes.WebSite, "https://chsakell.com"),
                    new Claim(JwtClaimTypes.Address, @"{ 'street_address': 'localhost 10', 'postal_code': 11146, 'country': 'Greece' }",
                        IdentityServer4.IdentityServerConstants.ClaimValueTypes.Json)
                }).Result;

                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }
                Console.WriteLine("chsakell created");
            }
            else
            {
                Console.WriteLine("chsakell already exists");
            }
#endregion

#region robin
            chsakell = userManager.FindByNameAsync("robin").Result;
            if (chsakell == null)
            {
                chsakell = new ApplicationUser
                {
                    UserName = "robin"
                };
                var result = userManager.CreateAsync(chsakell, "$Robin1").Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                chsakell = userManager.FindByNameAsync("robin").Result;

                result = userManager.AddClaimsAsync(chsakell, new Claim[]{
                    new Claim(JwtClaimTypes.Name, "Robin FakeLastName"),
                    new Claim(JwtClaimTypes.GivenName, "Robin"),
                    new Claim(JwtClaimTypes.FamilyName, "FakeLastName"),
                    new Claim(JwtClaimTypes.Email, "fakeemail@blog.com"),
                    new Claim(JwtClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean),
                    new Claim(JwtClaimTypes.WebSite, "https://greaserinc.com"),
                    new Claim(JwtClaimTypes.Address, @"{ 'street_address': '5 greaser way', 'postal_code': 30303, 'country': 'USA' }",
                        IdentityServer4.IdentityServerConstants.ClaimValueTypes.Json)
                }).Result;

                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }
                Console.WriteLine("robin created");
            }
            else
            {
                Console.WriteLine("robin already exists");
            }
#endregion

#region stephen
            chsakell = userManager.FindByNameAsync("stephen").Result;
            if (chsakell == null)
            {
                chsakell = new ApplicationUser
                {
                    UserName = "stephen"
                };
                var result = userManager.CreateAsync(chsakell, "$Stephen1").Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                chsakell = userManager.FindByNameAsync("stephen").Result;

                result = userManager.AddClaimsAsync(chsakell, new Claim[]{
                    new Claim(JwtClaimTypes.Name, "stephen FakeLastName"),
                    new Claim(JwtClaimTypes.GivenName, "stephen"),
                    new Claim(JwtClaimTypes.FamilyName, "FakeLastName"),
                    new Claim(JwtClaimTypes.Email, "fakeemail@blog.com"),
                    new Claim(JwtClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean),
                    new Claim(JwtClaimTypes.WebSite, "https://greaserinc.com"),
                    new Claim(JwtClaimTypes.Address, @"{ 'street_address': '5 greaser way', 'postal_code': 30303, 'country': 'USA' }",
                        IdentityServer4.IdentityServerConstants.ClaimValueTypes.Json)
                }).Result;

                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }
                Console.WriteLine("stephen created");
            }
            else
            {
                Console.WriteLine("stephen already exists");
            }
#endregion

#region brandon
            chsakell = userManager.FindByNameAsync("brandon").Result;
            if (chsakell == null)
            {
                chsakell = new ApplicationUser
                {
                    UserName = "brandon"
                };
                var result = userManager.CreateAsync(chsakell, "$Brandon1").Result;
                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }

                chsakell = userManager.FindByNameAsync("brandon").Result;

                result = userManager.AddClaimsAsync(chsakell, new Claim[]{
                    new Claim(JwtClaimTypes.Name, "brandon FakeLastName"),
                    new Claim(JwtClaimTypes.GivenName, "brandon"),
                    new Claim(JwtClaimTypes.FamilyName, "FakeLastName"),
                    new Claim(JwtClaimTypes.Email, "fakeemail@blog.com"),
                    new Claim(JwtClaimTypes.EmailVerified, "true", ClaimValueTypes.Boolean),
                    new Claim(JwtClaimTypes.WebSite, "https://greaserinc.com"),
                    new Claim(JwtClaimTypes.Address, @"{ 'street_address': '5 greaser way', 'postal_code': 30303, 'country': 'USA' }",
                        IdentityServer4.IdentityServerConstants.ClaimValueTypes.Json)
                }).Result;

                if (!result.Succeeded)
                {
                    throw new Exception(result.Errors.First().Description);
                }
                Console.WriteLine("brandon created");
            }
            else
            {
                Console.WriteLine("brandon already exists");
            }
#endregion
        }
    }
}
