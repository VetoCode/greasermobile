﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer
{
    public static class Constants
    {
        public const string LocalDBConnectionString = @"Data Source=.;database=GreaserIdentityServer;trusted_connection=yes;";
        public const string LocalBaseUrl = @"https://localhost:5001";
        public const string LocalApiBaseUrl = @"https://localhost:6002";

        public const string AzureDBConnectionString = "Server=tcp:greaserdb1.database.windows.net,1433;Initial Catalog=GreaserDb;Persist Security Info=False;User ID=greaseradmin;Password=Gr34s3r!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
        public const string AzureBaseUrl = "";
        public const string AzureApiBaseUrl = "";
    }
}
