﻿using System;

namespace GreaserApi.HubModels
{
    public class HubMessage
    {
        public int Id { get; set; }
        public int ConversationId { get; set; }
        public DateTime PhoneTimeStamp { get; set; }
        public DateTime ServerTimeStamp { get; set; }
        public int MessageOrder { get; set; }
        public string MessageText { get; set; }
        public int SendingUserId { get; set; }
        public string SendingUserName { get; set; }
        public string SendingDisplayName { get; set; }
        public bool IsPending { get; set; }
        public bool IsDeleted { get; set; }
    }
}
