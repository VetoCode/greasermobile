﻿using GreaserApi.HubModels;
using GreaserApi.Models;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GreaserApi.Hubs
{
    public class ChatHub : Hub<IChatClient>, IChatHub
    {
        public async Task SendMessage(HubMessage incomingMessage)
        {
            Console.WriteLine("Message received. Message contents: ");
            Console.WriteLine("Message order: " + incomingMessage.MessageOrder.ToString());
            Console.WriteLine("Message text: " + incomingMessage.MessageText.ToString());
            Console.WriteLine("Message id: " + incomingMessage.Id.ToString() + "\n\n");
            var serverMessage = ProcessIncomingChatMessage(incomingMessage);

            Console.WriteLine("Message processed. New message contents: ");
            Console.WriteLine("Message order: " + serverMessage.MessageOrder.ToString());
            Console.WriteLine("Message text: " + serverMessage.MessageText.ToString());
            Console.WriteLine("Message id: " + serverMessage.Id.ToString() + "\n\n");
            await Clients.All.ReceiveMessage(serverMessage);
        }

        public async Task SendMessageToCaller(string user, string message)
        {
            await Clients.Caller.ReceiveMessage(user, message);
        }

        public async Task LoadConversationList(int conversationId)
        {
            await Clients.Caller.ReceiveConversation(new List<Message>());
        }

        private HubMessage ProcessIncomingChatMessage(HubMessage incomingMessage)
        {
            incomingMessage.MessageOrder = incomingMessage.MessageOrder + 1;
            incomingMessage.ServerTimeStamp = DateTime.Now;
            incomingMessage.Id = new Random().Next();
            incomingMessage.IsPending = false;

            return incomingMessage;
        }
    }
}
