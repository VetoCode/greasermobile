﻿using GreaserApi.HubModels;
using GreaserApi.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GreaserApi.Hubs
{
    public interface IChatClient
    {
        Task ReceiveMessage(string user, string message);
        Task ReceiveMessage(HubMessage message);
        Task ReceiveConversation(List<Message> conversationFromHub);
    }
}
