﻿using GreaserApi.Hubs;
using GreaserApi.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace GreaserApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        private readonly IChatHub _greaserChatServer;

        MessageController(IChatHub greaserChatServer)
        {
            _greaserChatServer = greaserChatServer;
        }

        // GET: api/<MessageController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<MessageController>/5
        [HttpGet("{id}")]
        public string Get([FromQuery]int id)
        {
            return "value";
        }

        // GET api/<MessageController>/5
        [HttpGet("{id}")]
        public JsonResult Get([FromQuery] int userId, [FromQuery] int convoId)
        {
            List<Conversation> convo = GetMessagesByConvoId(userId, convoId);
            return new JsonResult(convo);
        }

        // GET api/<MessageController>/5
        [HttpGet("{id}")]
        public JsonResult Get([FromQuery] int userId, [FromQuery] int convoId, [FromQuery] int lastMessageId)
        {
            List<Conversation> convo = GetMessagesByConvoId(userId, convoId, lastMessageId);
            return new JsonResult(convo);
        }

        private List<Conversation> GetMessagesByConvoId([FromQuery] int userId, [FromQuery] int convoId)
        {
            return new List<Conversation>();
        }

        private List<Conversation> GetMessagesByConvoId([FromQuery] int userId, [FromQuery] int convoId, [FromQuery] int lastMessageId)
        {
            return new List<Conversation>();
        }

        // POST api/<MessageController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<MessageController>/5
        [HttpPut("{id}")]
        public void Put([FromQuery] int id, [FromBody] string value)
        {
        }

        // DELETE api/<MessageController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
