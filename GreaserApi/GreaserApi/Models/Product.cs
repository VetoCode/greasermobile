﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GreaserApi.Models
{
    public class Product
    {
        public StoreEnum Store { get; set; }
        public string Name { get; set; }
        public string ProductUrl { get; set; }
        public string Sku { get; set; }
        public string PartNumber { get; set; }
        public string ImageUrl { get; set; }
        public string CurrencyPrefix { get; set; }
        public decimal Price { get; set; }
        public List<ItemCategory> Categories { get; set; }
        public string FullBreadCrumb { get; set; }
        public DateTime lastModified { get; set; }
    }
}
