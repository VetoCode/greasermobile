﻿using System;
using System.Collections.Generic;

namespace GreaserApi.Models
{
    public class Conversation
    {
        public int ConversationId { get; set; }
        public List<Message> ConversationMessages { get; set; }
        public int RequestingUserId { get; set; }
        public int AssignedUserId { get; set; }
        public DateTime InitiatedDate { get; set; }
        public DateTime LastMessageDate { get; set; }
        public bool IsActive { get; set; }
        public int DaysSinceLastMessage { get; set; }
    }
}