﻿using System.Collections.Generic;

namespace GreaserApi.Models
{
    public class ReturnMessage
    {
        public List<string> ResponseList { get; set; }
        public ReturnMessage()
        {
        }
    }
}