﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GreaserApi.Models
{
    public class ItemCategory
    {
        public string Name { get; set; }
        public string CategoryUrl { get; set; }
        public string ParentCategoryName { get; set; }
        public string ParentCategoryUrl { get; set; }
    }
}
