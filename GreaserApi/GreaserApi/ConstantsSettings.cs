﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GreaserApi
{
    public static class Constants
    {
        public const string DevAuthServer = "https://localhost:5001";
        public const string AzureAuthServer = "https://greaserauth.azurewebsites.net/";
    }
}
