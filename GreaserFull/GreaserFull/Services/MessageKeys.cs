﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GreaserFull.Services
{
    public class MessageKeys
    {
        #region LoginMessageKeys

        public const string Login = "Login";

        public const string Logout = "Logout";

        public const string FailedLogin = "FailedLogin";

        public const string SessionStarted = "SessionStarted";

        public const string SessionEnded = "SessionEnded";

        #endregion

        #region ChatKeys

        public const string MessageRecieved = "MessageReceived";

        public const string ConversationReceived = "ConversationReceived";

        #endregion
    }
}
