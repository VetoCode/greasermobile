﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GreaserFull.Services
{
    public interface ISettingsService
    {
        string AuthAccessToken { get; set; }
        string AuthIdentityToken { get; set; }
        string AuthRefreshToken { get; set; }
        string Username { get; set; }
        string DisplayName { get; set; }
        string TokenEndpoint { get; set; }
        string AuthTokenExpiration { get; set; }
        string IntrospectionEndpoint { get; set; }
        string EndSessionEndpoint { get; set; }

        bool GetValueOrDefault(string key, bool defaultValue);
        string GetValueOrDefault(string key, string defaultValue);
        Task AddOrUpdateValue(string key, bool value);
        Task AddOrUpdateValue(string key, string value);
    }
}
