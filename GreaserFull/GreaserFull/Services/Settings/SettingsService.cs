﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace GreaserFull.Services
{
    public class SettingsService : ISettingsService
    {
        #region Settings Constants

        private const string AccessTokenKey = "access_token";
        private readonly string AccessTokenDefault = string.Empty;

        private const string IdentityTokenKey = "identity_token";
        private readonly string IdentityTokenDefault = string.Empty;

        private const string RefreshTokenKey = "refresh_token";
        private readonly string RefreshTokenDefault = string.Empty;

        private const string UsernameKey = "username";
        private readonly string DefaultUsername = "noUserNameSaved";

        private const string DisplayNameKey = "display_name";
        private readonly string DisplayNameDefault = string.Empty;

        private const string TokenEndpointUrlKey = "token_endpoint_url";
        private readonly string DefaultTokenEndpointUrl = string.Empty;

        private const string AuthTokenExpirationKey = "auth_token_expiration";
        private readonly string DefaultAuthTokenExpiration = string.Empty;

        private const string IntrospectionEndpointUrlKey = "introspection_token_endpoint_url";
        private readonly string DefaultIntrospectionEndpointUrl = string.Empty;

        private const string EndSessionEndpointKey = "end_session_endpoint";
        private readonly string EndSessionEndpointDefault = string.Empty;

        #endregion

        #region Settings Properties
        public string AuthAccessToken 
        {
            get => GetValueOrDefault(AccessTokenKey, AccessTokenDefault);
            set => AddOrUpdateValue(AccessTokenKey, value);
        }
        
        public string AuthIdentityToken 
        {
            get => GetValueOrDefault(IdentityTokenKey, IdentityTokenDefault);
            set => AddOrUpdateValue(IdentityTokenKey, value);
        }
        
        public string AuthRefreshToken
        {
            get => GetValueOrDefault(RefreshTokenKey, RefreshTokenDefault);
            set => AddOrUpdateValue(RefreshTokenKey, value);
        }
        
        public string Username
        {
            get => GetValueOrDefault(UsernameKey, DefaultUsername);
            set => AddOrUpdateValue(UsernameKey, value);
        }

        public string DisplayName
        {
            get => GetValueOrDefault(DisplayNameKey, DisplayNameDefault);
            set => AddOrUpdateValue(DisplayNameKey, value);
        }
        
        public string TokenEndpoint
        {
            get => GetValueOrDefault(TokenEndpointUrlKey, DefaultTokenEndpointUrl);
            set => AddOrUpdateValue(TokenEndpointUrlKey, value);
        }

        public string AuthTokenExpiration
        {
            get => GetValueOrDefault(AuthTokenExpirationKey, DefaultAuthTokenExpiration);
            set => AddOrUpdateValue(AuthTokenExpirationKey, value);
        }

        public string IntrospectionEndpoint
        {
            get => GetValueOrDefault(IntrospectionEndpointUrlKey, DefaultIntrospectionEndpointUrl);
            set => AddOrUpdateValue(IntrospectionEndpointUrlKey, value);
        }

        public string EndSessionEndpoint
        {
            get => GetValueOrDefault(EndSessionEndpointKey, EndSessionEndpointDefault);
            set => AddOrUpdateValue(EndSessionEndpointKey, value);
        }

        #endregion

        #region Methods

        public Task AddOrUpdateValue(string key, bool value) => AddOrUpdateValueInternal(key, value);
        public Task AddOrUpdateValue(string key, string value) => AddOrUpdateValueInternal(key, value);
        public bool GetValueOrDefault(string key, bool defaultValue) => GetValueOrDefaultInternal(key, defaultValue);
        public string GetValueOrDefault(string key, string defaultValue) => GetValueOrDefaultInternal(key, defaultValue);

        #endregion

        #region Internal Implementation

        async Task AddOrUpdateValueInternal<T>(string key, T value)
        {
            if (value == null)
            {
                await Remove(key);
            }

            Application.Current.Properties[key] = value;
            try
            {
                await Application.Current.SavePropertiesAsync();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to save: " + key, " Message: " + ex.Message);
            }
        }

        T GetValueOrDefaultInternal<T>(string key, T defaultValue = default(T))
        {
            object value = null;
            if (Application.Current.Properties.ContainsKey(key))
            {
                value = Application.Current.Properties[key];
            }
            return null != value ? (T)value : defaultValue;
        }

        async Task Remove(string key)
        {
            try
            {
                if (Application.Current.Properties[key] != null)
                {
                    Application.Current.Properties.Remove(key);
                    await Application.Current.SavePropertiesAsync();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to remove: " + key, " Message: " + ex.Message);
            }
        }

        #endregion
    }
}
