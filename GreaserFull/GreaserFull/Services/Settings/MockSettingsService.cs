﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GreaserFull.Services.Settings
{
    public class MockSettingsService : ISettingsService
    {
        Dictionary<string, object> _appSettings = new Dictionary<string, object>();

        #region Settings Constants

        private const string AccessTokenKey = "access_token";
        private readonly string AccessTokenDefault = string.Empty;

        private const string IdentityTokenKey = "identity_token";
        private readonly string IdentityTokenDefault = string.Empty;

        private const string RefreshTokenKey = "refresh_token";
        private readonly string RefreshTokenDefault = string.Empty;

        private const string UsernameKey = "username";
        private readonly string UsernameDefault = "noUserNameSaved";

        private const string DisplayNameKey = "display_name";
        private readonly string DisplayNameDefault = string.Empty;

        private const string TokenEndpointUrlKey = "token_endpoint_url";
        private readonly string TokenEndpointUrlDefault = string.Empty;

        private const string AuthTokenExpirationKey = "auth_token_expiration";
        private readonly string AuthTokenExpirationDefault = string.Empty;

        private const string IntrospectionEndpointUrlKey = "introspection_token_endpoint_url";
        private readonly string IntrospectionEndpointUrlDefault = string.Empty;

        private const string EndSessionEndpointKey = "end_session_endpoint";
        private readonly string EndSessionEndpointDefault = string.Empty;

        #endregion

        #region Settings Properties
        public string AuthAccessToken
        {
            get => GetValueOrDefault(AccessTokenKey, AccessTokenDefault);
            set => AddOrUpdateValue(AccessTokenKey, value);
        }

        public string AuthIdentityToken
        {
            get => GetValueOrDefault(IdentityTokenKey, IdentityTokenDefault);
            set => AddOrUpdateValue(IdentityTokenKey, value);
        }

        public string AuthRefreshToken
        {
            get => GetValueOrDefault(RefreshTokenKey, RefreshTokenDefault);
            set => AddOrUpdateValue(RefreshTokenKey, value);
        }

        public string Username
        {
            get => GetValueOrDefault(UsernameKey, UsernameDefault);
            set => AddOrUpdateValue(UsernameKey, value);
        }

        public string DisplayName
        {
            get => GetValueOrDefault(DisplayNameKey, DisplayNameDefault);
            set => AddOrUpdateValue(DisplayNameKey, value);
        }

        public string TokenEndpoint
        {
            get => GetValueOrDefault(TokenEndpointUrlKey, TokenEndpointUrlDefault);
            set => AddOrUpdateValue(TokenEndpointUrlKey, value);
        }

        public string IntrospectionEndpoint
        {
            get => GetValueOrDefault(IntrospectionEndpointUrlKey, IntrospectionEndpointUrlDefault);
            set => AddOrUpdateValue(IntrospectionEndpointUrlKey, value);
        }

        public string AuthTokenExpiration
        {
            get => GetValueOrDefault(AuthTokenExpirationKey, AuthTokenExpirationDefault);
            set => AddOrUpdateValue(AuthTokenExpirationKey, value);
        }

        public string EndSessionEndpoint
        {
            get => GetValueOrDefault(EndSessionEndpointKey, EndSessionEndpointDefault);
            set => AddOrUpdateValue(EndSessionEndpointKey, value);
        }
        #endregion

        #region Methods

        public Task AddOrUpdateValue(string key, bool value) => AddOrUpdateValueInternal(key, value);
        public Task AddOrUpdateValue(string key, string value) => AddOrUpdateValueInternal(key, value);
        public bool GetValueOrDefault(string key, bool defaultValue) => GetValueOrDefaultInternal(key, defaultValue);
        public string GetValueOrDefault(string key, string defaultValue) => GetValueOrDefaultInternal(key, defaultValue);

        #endregion

        #region Internal Implementation

        Task AddOrUpdateValueInternal<T>(string key, T value)
        {
            if (value == null)
            {
                Remove(key);
            }

            _appSettings[key] = value;
            return Task.Delay(10);
        }

        T GetValueOrDefaultInternal<T>(string key, T defaultValue = default)
        {
            object value = null;
            if (_appSettings.ContainsKey(key))
            {
                value = _appSettings[key];
            }
            return null != value ? (T)value : defaultValue;
        }

        void Remove(string key)
        {
            if (_appSettings.ContainsKey(key))
            {
                _appSettings.Remove(key);
            }
        }

        #endregion
    }
}
