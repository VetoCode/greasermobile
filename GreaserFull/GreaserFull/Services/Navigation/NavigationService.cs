﻿using GreaserFull.Services.Authentication;
using GreaserFull.ViewModels;
using GreaserFull.Views;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace GreaserFull.Services
{
    public class NavigationService : INavigationService
    {
        private readonly IAuthenticationService _authenticationService;
        public BaseViewModel PreviousPageViewModel => throw new NotImplementedException();
        public NavigationService(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
            MessagingCenter.Subscribe<IAuthenticationService>(this, MessageKeys.Logout, async (sender) =>
            {
                await InitializeAsync();
            });
        }

        public Task InitializeAsync()
        {
            if (!_authenticationService.IsAuthenticated)
                return NavigateToAsync<LoginViewModel>();
            else
                return NavigateToAsync<HomeViewModel>();
        }

        public Task NavigateToAsync<TViewModel>() where TViewModel : BaseViewModel
        {
            return InternalNavigateToAsync(typeof(TViewModel), null);
        }

        public Task NavigateToAsync<TViewModel>(object parameter) where TViewModel : BaseViewModel
        {
            return InternalNavigateToAsync(typeof(TViewModel), parameter);
        }

        public Task NavigateToAsync<TPage>(TPage page, object parameter) where TPage : ContentPage
        {
            return InternalNavigateToAsync(page, parameter);
        }

        public Task RemoveBackStackAsync()
        {
            //Not fully functional. Not worth the time now.
            var rootNavPage = Application.Current.MainPage as RootNavigationPage;
            if (rootNavPage != null)
                for(int i = 0; i <rootNavPage.Navigation.NavigationStack.Count - 1; i++)
                {
                    var page = rootNavPage.Navigation.NavigationStack[i];
                    rootNavPage.Navigation.RemovePage(page);
                }

            return Task.FromResult(true);
        }

        public Task RemoveLastFromBackStackAsync()
        {
            var rootNavPage = Application.Current.MainPage as RootNavigationPage;
            if (rootNavPage != null)
            {
                rootNavPage.Navigation.RemovePage(rootNavPage.Navigation.NavigationStack[rootNavPage.Navigation.NavigationStack.Count - 2]);
            }

            return Task.FromResult(true);
        }

        private async Task InternalNavigateToAsync(Type viewModelType, object parameter)
        {
            Page page = CreatePage(viewModelType);

            if(page is LoginPage)
            {
                Application.Current.MainPage = new RootNavigationPage(page);
            }
            else
            {
                var navRootPage = Application.Current.MainPage as RootNavigationPage;
                if(navRootPage != null)
                {
                    await navRootPage.PushAsync(page);
                }
                else
                {
                    Application.Current.MainPage = new RootNavigationPage(page);
                }

                if (parameter != null)
                {
                    await (page.BindingContext as BaseViewModel).InitializeAsync(parameter);
                }
            }
        }

        private async Task InternalNavigateToAsync(Page page, object parameter)
        {

            if (page is LoginPage)
            {
                Application.Current.MainPage = new RootNavigationPage(page);
            }
            else
            {
                var navRootPage = Application.Current.MainPage as RootNavigationPage;
                if( navRootPage != null)
                {
                    await navRootPage.PushAsync(page);
                }
                else
                {
                    Application.Current.MainPage= new RootNavigationPage(page);
                }
            }

            if (parameter != null)
            {
                await (page.BindingContext as BaseViewModel).InitializeAsync(parameter);
            }
        }

        private Type GetPageTypeForViewModel(Type viewModelType)
        {
            var viewName = viewModelType.FullName.Replace("ViewModels", "Views").Replace("ViewModel", "Page");
            var viewModelAssemblyName = viewModelType.GetTypeInfo().Assembly.FullName;
            if (viewModelAssemblyName == null)
            {
                throw new Exception($"Cannot locate page type for viewName: {viewName}");
            }

            var viewAssemblyName = string.Format(CultureInfo.InvariantCulture, "{0}, {1}", viewName, viewModelAssemblyName);
            if (viewAssemblyName == null)
            {
                throw new Exception($"Cannot locate page type for viewName: {viewName} \n viewModelAssemblyName: {viewModelAssemblyName}");
            }

            var viewType = Type.GetType(viewAssemblyName);
            if(viewType == null)
            {
                throw new Exception($"Cannot locate page type for viewName: {viewName} \n viewModelAssemblyName: {viewModelAssemblyName} \n viewAssemblyName {viewAssemblyName}");
            }
            return viewType;
        }

        private Page CreatePage(Type viewModelType)
        {
            Type pageType = GetPageTypeForViewModel(viewModelType);
            if(pageType == null)
            {
                throw new Exception($"Cannot locate page type for {viewModelType}");
            }

            Page page = Activator.CreateInstance(pageType) as Page;
            return page;
        }
    }
}
