﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GreaserFull.Services.Location
{
    public interface ILocationService
    {
        Task UpdateUserLocation(string newLocation);
    }
}
