﻿using GreaserFull.Models;
using GreaserFull.Services.Authentication;
using GreaserFull.Services.Chat;
using GreaserFull.Services.Session;
using GreaserFull.Services.Settings;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using TinyIoC;

namespace GreaserFull.Services
{
    public static class ServiceLocator
    {
        private static TinyIoCContainer _container;

        static ServiceLocator()
        {
            _container = new TinyIoCContainer();

            _container.Register<IDataStore<Item>,MockDataStore>();
            _container.Register<IDataStore<Conversation>,ConversationsDatastore >();
            _container.Register<IDataStore<Message>, MessagesDatastore>();
            _container.Register<ISettingsService, SettingsService>();
            _container.Register<INavigationService, NavigationService>();
            _container.Register<IAuthenticationService, OidcAuthenticationService>();
            _container.Register<IChatService, ChatService>();
            _container.Register<HttpClient>();
            _container.Register<SessionService>().AsSingleton();
             
        }

        public static void UpdateDependenciesForLocalTests(bool useMockDataServices)
        {
            if (useMockDataServices)
            {
                ServiceLocator.Register<ISettingsService, MockSettingsService>();
                ServiceLocator.Register<IAuthenticationService, MockAuthenticationService>();
            }
        }

        public static void UpdateDependenciesForIntegrationTests(bool useLimitedMockServices)
        {
            if (useLimitedMockServices)
            {
                ServiceLocator.Register<ISettingsService, MockSettingsService>();
            }

        }

        public static void Register<IT, T>() where IT : class where T : class, IT
        {
                _container.Register<IT, T>();
        }

        public static void RegisterAsSingleton<IT, T>() where IT : class where T : class, IT
        {
            _container.Register<IT, T>().AsSingleton();
        }

        public static T Resolve<T>() where T : class
        {
            return _container.Resolve<T>();
        }
    }
}
