﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GreaserFull.Services.Authentication
{
    public enum LogoutReason
    {
        Timer_expired,
        User_initiated
    }
}
