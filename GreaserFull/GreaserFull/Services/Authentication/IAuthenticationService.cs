﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using IdentityModel;
using IdentityModel.Client;

namespace GreaserFull.Services.Authentication
{
    public interface IAuthenticationService
    {
        bool IsAuthenticated { get; set; }
        Task<bool> LoginAsync(string username, string password);
        Task<bool> LogoutAsync();
        Task<bool> RefreshTokenAsync();
        bool RetrieveUserClaims();
        Task<DiscoveryDocumentResponse> GetDiscoveryTokenFromAuthServer();
        TokenIntrospectionResponse IntrospectAccessToken();
    }
}
