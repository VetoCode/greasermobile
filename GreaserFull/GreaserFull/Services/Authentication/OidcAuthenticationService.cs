﻿using GreaserFull.Extensions;
using GreaserFull.Services.Session;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace GreaserFull.Services.Authentication
{
    public class OidcAuthenticationService : IAuthenticationService
    {
        
        ISettingsService settingsService;
        HttpClient client;
        public OidcAuthenticationService(ISettingsService settingsService)
        {
            this.settingsService = settingsService;
            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.ServerCertificateCustomValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
            client = new HttpClient(clientHandler);
            MessagingCenter.Subscribe<SessionService, LogoutReason>(this, MessageKeys.SessionEnded, async (sender, reason) =>
            {
                if(reason == LogoutReason.Timer_expired)
                    await RefreshTokenAsync();
            });

        }

        public bool IsAuthenticated { get; set; }

        public async Task<bool> LoginAsync(string username, string password)
        {
            var discoDocument = await GetDiscoveryTokenFromAuthServer();

            if (SetDiscoveryTokenSettings(discoDocument))
            {
                var passwordTokenRequest = CreatePasswordTokenRequest(username, password);
                var tokenResponse = client.RequestPasswordTokenAsync(passwordTokenRequest).Result;
                if (tokenResponse.IsError)
                {
                    ProcessLogout();
                }
                else
                {
                    ProcessLogin(tokenResponse);
                    return true;
                }
            }
            return false;
        }

        public async Task<bool> LogoutAsync()
        {
            string endSessionUrl = CreateEndSessionUrl();
            HttpResponseMessage responseMessage = await client.GetAsync(endSessionUrl);
            
            if (responseMessage.IsSuccessStatusCode)
            {
                ProcessLogout();
                return true;
            }
            
            return false;
        }

        public async Task<bool> RefreshTokenAsync()
        {
            if (string.IsNullOrEmpty(settingsService.AuthRefreshToken))
                return false;

            var refreshTokenRequest = CreateRefreshTokenRequest();
            var tokenResponse = await client.RequestRefreshTokenAsync(refreshTokenRequest);
            if (tokenResponse.IsError)
            {
                ProcessLogout();
            }
            else
            {
                ProcessLogin(tokenResponse);
                return true;
            }
            return false;
        }

        public bool RetrieveUserClaims()
        {
            client.SetBearerToken(settingsService.AuthAccessToken);
            var response = client.GetAsync(Constants.UserClaimsUrl).Result;
            if (!response.IsSuccessStatusCode)
            {
                response.StatusCode.ToString();
                settingsService.DeleteUserSettings();
                return false;
            }
            else
            {
                var content = response.Content.ReadAsStringAsync().Result;
                settingsService.SetUserSettings(content);
                return true;
            }
        }

        public TokenIntrospectionResponse IntrospectAccessToken()
        {
            TokenIntrospectionRequest request = CreateIntrospectionTokenRequest();
            return client.IntrospectTokenAsync(request).Result;
        }

        #region Utility Functions

        public async Task<DiscoveryDocumentResponse> GetDiscoveryTokenFromAuthServer()
        {
            return await client.GetDiscoveryDocumentAsync(Constants.DevAuthServer);
            
        }

        private bool SetDiscoveryTokenSettings(DiscoveryDocumentResponse tokenResponse)
        {
            if (tokenResponse == null)
                return false;

            if (tokenResponse.IsError)
            {
                settingsService.TokenEndpoint = null;
                settingsService.IntrospectionEndpoint = null;
                settingsService.EndSessionEndpoint = null;
                return false;
            }
            else
            {
                settingsService.TokenEndpoint = tokenResponse?.TokenEndpoint ?? "something went wrong";
                settingsService.IntrospectionEndpoint = tokenResponse?.IntrospectionEndpoint ?? "something went wrong";
                settingsService.EndSessionEndpoint = tokenResponse?.EndSessionEndpoint ?? "Something went wrong";
                return true;
            }
        }

        private PasswordTokenRequest CreatePasswordTokenRequest(string username, string password)
        {
            return new PasswordTokenRequest
            {
                Address = settingsService.TokenEndpoint,
                ClientId = Constants.ClientId,
                ClientSecret = Constants.ClientSecret,
                Scope = Constants.ApiScopes,
                UserName = username,
                Password = password
            };
        }

        private TokenIntrospectionRequest CreateIntrospectionTokenRequest()
        {
            return new TokenIntrospectionRequest {
                Address = settingsService.IntrospectionEndpoint,
                ClientId = Constants.ClientId,
                ClientSecret = Constants.ClientSecret,
                Token = settingsService.AuthAccessToken
            };
        }

        private RefreshTokenRequest CreateRefreshTokenRequest()
        {
            return new RefreshTokenRequest()
            {
                Address = settingsService.TokenEndpoint,
                ClientId = Constants.ClientId,
                ClientSecret = Constants.ClientSecret,
                RefreshToken = settingsService.AuthRefreshToken
            };
        }

        private string CreateEndSessionUrl()
        {
            RequestUrl requestUrl = new RequestUrl(settingsService.EndSessionEndpoint);
            return requestUrl.CreateEndSessionUrl(
                idTokenHint: settingsService.AuthIdentityToken,
                postLogoutRedirectUri: Constants.DevAuthServer
                );
        }

        private void DeleteAuthTokenSettings()
        {
            settingsService.DeleteAuthTokenSettings();
        }

        private void SetAuthTokenSettings(TokenResponse tokenResponse)
        {
            settingsService.SetAuthTokenSettings(tokenResponse);
        }

        private void ProcessLogout()
        {
            DeleteAuthTokenSettings();
            IsAuthenticated = false;
            MessagingCenter.Send<IAuthenticationService>(this, MessageKeys.Logout);
        }

        private void ProcessLogin(TokenResponse tokenResponse)
        {
            SetAuthTokenSettings(tokenResponse);
            IsAuthenticated = true;
            MessagingCenter.Send(this, MessageKeys.Login, tokenResponse.ExpiresIn.ToString());
        }

        #endregion
    }
}
