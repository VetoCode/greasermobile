﻿using IdentityModel.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace GreaserFull.Services.Authentication
{
    public class MockAuthenticationService : IAuthenticationService
    {
        private bool _isAuthenticated = false;
        public bool IsAuthenticated { get => _isAuthenticated; set => _isAuthenticated = value; }

        public Task<DiscoveryDocumentResponse> GetDiscoveryTokenFromAuthServer()
        {
            throw new NotImplementedException();
        }

        public TokenIntrospectionResponse IntrospectAccessToken()
        {
            throw new NotImplementedException();
        }

        public async Task<bool> LoginAsync(string username, string password)
        {
            MessagingCenter.Send(this, MessageKeys.Login, 500);
            IsAuthenticated = true;
            return true;
        }

        public async Task<bool> LogoutAsync()
        {
            MessagingCenter.Send(this, MessageKeys.Logout);
            IsAuthenticated = false;
            return true;
        }

        public async Task<bool> RefreshTokenAsync()
        {
            MessagingCenter.Send(this, MessageKeys.Login, 500);
            return true;
        }

        public bool RetrieveUserClaims()
        {
            throw new NotImplementedException();
        }
    }
}
