﻿using GreaserFull.Services.Authentication;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace GreaserFull.Services.Session
{
    public class SessionService
    {
        public Action OnSessionStarted;

        public Action<LogoutReason> OnSessionExpired;

        public SessionService()
        {
            MessagingCenter.Unsubscribe<IAuthenticationService, int>(this, MessageKeys.Login);
            MessagingCenter.Subscribe<IAuthenticationService, int>(this, MessageKeys.Login, (sender, args) =>
            {
               SetSessionDuration(args);
            });

            MessagingCenter.Unsubscribe<IAuthenticationService, LogoutReason>(this, MessageKeys.Logout);
            MessagingCenter.Subscribe<IAuthenticationService, LogoutReason>(this, MessageKeys.Logout, (sender, reason) =>
            {
                EndSession(reason);
            });
        }

        public TimeSpan SessionDuration { get; private set; }

        public DateTime SessionExpirationTime { get; private set; }

        void SetSessionDuration(int timeInSeconds)
        {
            SessionDuration = TimeSpan.FromSeconds(timeInSeconds);
            this.SessionExpirationTime = DateTime.Now.Add(this.SessionDuration);
            if (timeInSeconds > 0)
                StartSessionTimerAsync();
        }

        public void EndSession(LogoutReason reason)
        {
            OnSessionExpired?.Invoke(reason);
            SetSessionDuration(0);
            MessagingCenter.Send(this, MessageKeys.SessionEnded, reason);
        }

        // Function never called. May convert to MessagingCenter to remove dependency.
        void ExtendSession()
        {
            ServiceLocator.Resolve<IAuthenticationService>().RefreshTokenAsync();
            if (DateTime.Now < this.SessionExpirationTime)
                EndSession(LogoutReason.Timer_expired);
        }

        void StartSessionTimerAsync()
        {
            OnSessionStarted?.Invoke();

            MessagingCenter.Send(this, MessageKeys.SessionStarted);
            Xamarin.Forms.Device.StartTimer(new TimeSpan(0,0,3), () =>
            {
                if (DateTime.Now > this.SessionExpirationTime)
                {
                    Xamarin.Forms.Device.BeginInvokeOnMainThread(() =>
                    {
                        EndSession(LogoutReason.Timer_expired);
                    });
                    return false;
                }
                return true;
            });
        }
    }
}
