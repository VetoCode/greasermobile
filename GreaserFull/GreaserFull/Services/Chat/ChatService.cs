﻿using GreaserFull.Extensions;
using GreaserFull.HubModels;
using GreaserFull.Models;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace GreaserFull.Services.Chat
{
    public class ChatService : IChatService
    {
        HubConnection chathubConnection;
        ISettingsService settingsService;
        IDataStore<Conversation> convoDataStore;
        IDataStore<Message> messageDataStore;
        ObservableCollection<Message> conversation;

        public ChatService(ISettingsService settings, IDataStore<Conversation> convoStore, IDataStore<Message> messageStore)
        {
            this.settingsService = settings;
            this.convoDataStore = convoStore;
            this.messageDataStore = messageStore;

            InitializeChatHub();
        }

        #region Chathub Functions

        public Task InitializeChatHub()
        {
            chathubConnection = new HubConnectionBuilder()
                .WithUrl(Constants.ChatApiUrl, options =>
                {
                    options.AccessTokenProvider = () => Task.FromResult(settingsService.AuthAccessToken);
                })
                .WithAutomaticReconnect()
                .Build();

            chathubConnection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(1, 6) * 1000);
                await chathubConnection.StartAsync();
            };

            chathubConnection.Reconnecting += error =>
            {

                return Task.CompletedTask;
            };

            chathubConnection.Reconnected += connectionId =>
            {

                return Task.CompletedTask;
            };

            chathubConnection.On<HubMessage>("ReceiveMessage", ReceiveMessage);

            chathubConnection.On<Conversation>("ReceiveConversation", ReceiveConversation);

            return chathubConnection.StartAsync();
        }

        public async Task SendMessage(HubMessage userMessage) {
            await chathubConnection.InvokeAsync("SendMessage", userMessage);
        }

        public async Task GetConversation(int conversationId)
        {
            await chathubConnection.InvokeAsync("LoadConversation", conversationId);
        }

        public async Task ReceiveMessage(HubMessage incomingHubMessage)
        {
            Message incomingMessage = new Message()
            {
                ConversationId = incomingHubMessage.ConversationId,
                Id = incomingHubMessage.Id,
                MessageOrder = incomingHubMessage.MessageOrder,
                MessageText = incomingHubMessage.MessageText,
                SendingDisplayName = incomingHubMessage.SendingDisplayName,
                SendingUserName = incomingHubMessage.SendingUserName,
                ServerTimeStamp = incomingHubMessage.ServerTimeStamp,
            };
            bool isSuccess = await messageDataStore.AddAsync(incomingMessage);
            if (isSuccess)
            {
                MessagingCenter.Send(this, MessageKeys.MessageRecieved, incomingMessage);
            }
        }

        public async Task ReceiveConversation(Conversation incomingConversation)
        {
            bool isSuccess = await convoDataStore.AddAsync(incomingConversation);
            if (isSuccess)
            {
                MessagingCenter.Send(this, MessageKeys.ConversationReceived, incomingConversation.Id);
            }
        }

        public HubConnectionState ConnectionState()
        {
            return chathubConnection.State;
        }

        #endregion

        #region Storage Functions

        public async Task<IEnumerable<Message>> LoadChat(int conversationId)
        {
            return await messageDataStore.GetListAsync(true, conversationId);
        }

        public async Task<IEnumerable<Conversation>> LoadChatList()
        {
            return await convoDataStore.GetListAsync(true);
        }

        public async Task<IEnumerable<Message>> LoadNewMessages(int conversationId, int lastMessageNumber)
        {
            var allMessages = await messageDataStore.GetListAsync(id: conversationId);
            return allMessages.Where(m => m.MessageOrder > lastMessageNumber).OrderBy(m => m.MessageOrder);

        }

        #endregion
    }
}
