﻿using GreaserFull.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using Xamarin.Essentials;

namespace GreaserFull.Services
{
    public static class ChatApiConnector
    {
        public static Conversation LoadChatFeed(string url)
        {
            WebRequest request = WebRequest.Create(url);
            request.ContentType = "application/json; charset=utf-8";
            var response = (HttpWebResponse)request.GetResponse();
            string jsonResults;
            using (var sr = new StreamReader(response.GetResponseStream()))
            {
                jsonResults = sr.ReadToEnd();
            }

            var resultConvo = JsonConvert.DeserializeObject<Conversation>(jsonResults);
            //IsRefreshing = false;
            return resultConvo;
        }

        #region ConversationFunctions

        public static Conversation GetConverstionList()
        {
            // Connectivity.NetworkAccess == NetworkAccess.None;
            return new Conversation();
        }

        #endregion

        #region MessageFunctions

        public static Conversation GetMessagesList(int convoId)
        {
            return new Conversation();
        }

        #endregion
    }
}
