﻿using GreaserFull.HubModels;
using GreaserFull.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GreaserFull.Services.Chat
{
    public interface IChatService
    {
        Task InitializeChatHub();
        Task SendMessage(HubMessage message);
        Task GetConversation(int conversationId);
        Task ReceiveMessage(HubMessage incomingMessage);
        Task ReceiveConversation(Conversation incomingConversation);
        Task<IEnumerable<Message>> LoadChat(int conversationId);
        Task<IEnumerable<Conversation>> LoadChatList();
        Task<IEnumerable<Message>> LoadNewMessages(int conversationId, int lastMessageNumber);
    }
}
