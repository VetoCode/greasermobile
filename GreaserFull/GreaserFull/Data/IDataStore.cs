﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GreaserFull.Services
{
    public interface IDataStore<T>
    {
        Task<bool> AddAsync(T item);
        Task<bool> UpdateAsync(T item);
        Task<bool> DeleteAsync(int id);
        Task<T> GetAsync(int id);
        Task<IEnumerable<T>> GetListAsync(bool forceRefresh = false, int id = -1);
        Task<int> GenerateNextId();
    }
}
