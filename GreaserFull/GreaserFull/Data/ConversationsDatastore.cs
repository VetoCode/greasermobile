﻿using GreaserFull.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace GreaserFull.Services
{

    public class ConversationsDatastore : IDataStore<Conversation>
    {
        readonly List<Conversation> conversations;


        public ConversationsDatastore()
        {
            conversations = new List<Conversation>() {
                new Conversation{ Id = 1, IsCompleted = false, RequestingUserId = 101, AssignedUserId = 2 },
                new Conversation{ Id = 2, IsCompleted = false, RequestingUserId = 101, AssignedUserId = 3 },
                new Conversation{ Id = 3, IsCompleted = false, RequestingUserId = 101, AssignedUserId = 4 }
            };
            
        }

        public async Task<bool> AddAsync(Conversation item)
        {
            conversations.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var oldItem = conversations.Where((Conversation arg) => arg.Id == id).FirstOrDefault();
            conversations.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<int> GenerateNextId()
        {
            return await Task.FromResult(conversations.Count);
        }

        public async Task<Conversation> GetAsync(int id)
        {
            return await Task.FromResult(conversations.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Conversation>> GetListAsync(bool forceRefresh = false, int conversationId = -1)
        {
            if (conversationId == -1)
                return await Task.FromResult(conversations);
            else
                return await Task.FromResult(conversations.Where(s => s.Id == conversationId));
        }

        public async Task<bool> UpdateAsync(Conversation item)
        {
            var oldItem = conversations.Where((Conversation arg) => arg.Id == item.Id).FirstOrDefault();
            conversations.Remove(oldItem);
            conversations.Add(item);

            return await Task.FromResult(true);
        }
    }
}
