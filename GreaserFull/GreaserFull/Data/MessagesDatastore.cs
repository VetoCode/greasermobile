﻿using GreaserFull.Models;
using GreaserFull.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreaserFull.Services
{
    public class MessagesDatastore : IDataStore<Message>
    {
        readonly ObservableCollection<Message> messages;
        public MessagesDatastore()
        {
            messages = new ObservableCollection<Message>()
            {
                new Message { ConversationId = 1, Id = 1, MessageOrder = 1, SendingUserId = 101,  IsDeleted=false, MessageText = "Convo 1 Message 1", PhoneTimeStamp = DateTime.Now, SendingUserName = "Veto", ServerTimeStamp = DateTime.UtcNow},
                new Message { ConversationId = 1, Id = 2, MessageOrder = 2, SendingUserId = 2,  IsDeleted=false, MessageText = "Convo 1 Message 2", PhoneTimeStamp = DateTime.Now, SendingUserName = "NotVeto", ServerTimeStamp = DateTime.UtcNow},
                new Message { ConversationId = 1, Id = 3, MessageOrder = 3, SendingUserId = 101,  IsDeleted=false, MessageText = "Convo 1 Message 3", PhoneTimeStamp = DateTime.Now, SendingUserName = "Veto", ServerTimeStamp = DateTime.UtcNow},
                new Message { ConversationId = 1, Id = 4, MessageOrder = 4, SendingUserId = 2,  IsDeleted=false, MessageText = "Convo 1 Message 4", PhoneTimeStamp = DateTime.Now, SendingUserName = "NotVeto", ServerTimeStamp = DateTime.UtcNow},
                new Message { ConversationId = 1, Id = 5, MessageOrder = 5, SendingUserId = 101,  IsDeleted=false, MessageText = "Convo 1 Message 5", PhoneTimeStamp = DateTime.Now, SendingUserName = "Veto", ServerTimeStamp = DateTime.UtcNow},
                new Message { ConversationId = 1, Id = 6, MessageOrder = 6, SendingUserId = 2,  IsDeleted=false, MessageText = "Convo 1 Message 6", PhoneTimeStamp = DateTime.Now, SendingUserName = "NotVeto", ServerTimeStamp = DateTime.UtcNow},
                new Message { ConversationId = 2, Id = 1, MessageOrder = 1, SendingUserId = 101,  IsDeleted=false, MessageText = "Convo 2 Message 1", PhoneTimeStamp = DateTime.Now, SendingUserName = "Veto", ServerTimeStamp = DateTime.UtcNow},
                new Message { ConversationId = 2, Id = 2, MessageOrder = 2, SendingUserId = 3,  IsDeleted=false, MessageText = "Convo 2 Message 2", PhoneTimeStamp = DateTime.Now, SendingUserName = "AlsoNotVeto", ServerTimeStamp = DateTime.UtcNow},
                new Message { ConversationId = 2, Id = 3, MessageOrder = 3, SendingUserId = 101,  IsDeleted=false, MessageText = "Convo 2 Message 3", PhoneTimeStamp = DateTime.Now, SendingUserName = "Veto", ServerTimeStamp = DateTime.UtcNow},
                new Message { ConversationId = 2, Id = 4, MessageOrder = 4, SendingUserId = 3,  IsDeleted=false, MessageText = "Convo 2 Message 4", PhoneTimeStamp = DateTime.Now, SendingUserName = "AlsoNotVeto", ServerTimeStamp = DateTime.UtcNow},
                new Message { ConversationId = 2, Id = 5, MessageOrder = 5, SendingUserId = 101,  IsDeleted=false, MessageText = "Convo 2 Message 5", PhoneTimeStamp = DateTime.Now, SendingUserName = "Veto", ServerTimeStamp = DateTime.UtcNow},
                new Message { ConversationId = 2, Id = 6, MessageOrder = 6, SendingUserId = 3,  IsDeleted=false, MessageText = "Convo 2 Message 6", PhoneTimeStamp = DateTime.Now, SendingUserName = "AlsoNotVeto", ServerTimeStamp = DateTime.UtcNow},
                new Message { ConversationId = 3, Id = 1, MessageOrder = 1, SendingUserId = 101,  IsDeleted=false, MessageText = "Convo 3 Message 1", PhoneTimeStamp = DateTime.Now, SendingUserName = "Veto", ServerTimeStamp = DateTime.UtcNow},
                new Message { ConversationId = 3, Id = 2, MessageOrder = 2, SendingUserId = 3,  IsDeleted=false, MessageText = "Convo 3 Message 2", PhoneTimeStamp = DateTime.Now, SendingUserName = "AlsoNotVeto", ServerTimeStamp = DateTime.UtcNow},
                new Message { ConversationId = 3, Id = 3, MessageOrder = 3, SendingUserId = 101,  IsDeleted=false, MessageText = "Convo 3 Message 3", PhoneTimeStamp = DateTime.Now, SendingUserName = "Veto", ServerTimeStamp = DateTime.UtcNow},
                new Message { ConversationId = 3, Id = 4, MessageOrder = 4, SendingUserId = 3,  IsDeleted=false, MessageText = "Convo 3 Message 4", PhoneTimeStamp = DateTime.Now, SendingUserName = "AlsoNotVeto", ServerTimeStamp = DateTime.UtcNow},
                new Message { ConversationId = 3, Id = 5, MessageOrder = 5, SendingUserId = 101,  IsDeleted=false, MessageText = "Convo 3 Message 5", PhoneTimeStamp = DateTime.Now, SendingUserName = "Veto", ServerTimeStamp = DateTime.UtcNow},
                new Message { ConversationId = 3, Id = 6, MessageOrder = 6, SendingUserId = 4,  IsDeleted=false, MessageText = "Convo 3 Message 6", PhoneTimeStamp = DateTime.Now, SendingUserName = "StillNotVeto", ServerTimeStamp = DateTime.UtcNow}

            };
        }

        public async Task<bool> AddAsync(Message item)
        {
            messages.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var oldItem = messages.Where((Message arg) => arg.Id == id).FirstOrDefault();
            messages.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<int> GenerateNextId()
        {
            return await Task.FromResult(messages.Count);
        }

        public async Task<Message> GetAsync(int id)
        {
            return await Task.FromResult(messages.Where(s => s.ConversationId == id).FirstOrDefault());
        }

        public async Task<IEnumerable<Message>> GetListAsync(bool forceRefresh = false, int conversationId = -1)
        { 
            return await Task.FromResult(messages.Where(s => s.ConversationId == conversationId).OrderBy(m => m.MessageOrder));
        }

        public async Task<bool> UpdateAsync(Message item)
        {
            var oldItem = messages.Where((Message arg) => arg.Id == item.Id).FirstOrDefault();
            messages.Remove(oldItem);
            messages.Add(item);

            return await Task.FromResult(true);
        }
    }
}
