﻿using GreaserFull.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace GreaserFull.Data.DatabaseExtensions
{
    public static partial class GreaserFullDatabaseExtensions
    {
        public static Task<List<Conversation>> GetConversationsAsync(this SQLiteAsyncConnection database)
        {
            return database.Table<Conversation>().ToListAsync();
        }
        public static Task<List<Conversation>> GetConversationsNotCompletedAsync(this SQLiteAsyncConnection database)
        {
            return database.QueryAsync<Conversation>("SELECT * FROM [Conversation] WHERE [Completed] = 0");
        }
        public static Task<Conversation> GetConversationByIdAsync(this SQLiteAsyncConnection database, int id)
        {
            return database.Table<Conversation>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }
        public static Task<Conversation> GetMessageByIdAsync(this SQLiteAsyncConnection database, int id)
        {
            return database.Table<Conversation>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }
        public static Task<int> SaveConversationAsync(this SQLiteAsyncConnection database, Conversation conversation)
        {
            if (conversation.Id != 0)
            {
                return database.UpdateAsync(conversation);
            }
            else
            {
                return database.InsertAsync(conversation);
            }
        }
        public static Task<int> SaveMessageAsync(this SQLiteAsyncConnection database, Message message)
        {
            if (message.Id != 0)
            {
                return database.UpdateAsync(message);
            }
            else
            {
                return database.InsertAsync(message);
            }
        }
        public static Task<int> DeleteConversationAsync(this SQLiteAsyncConnection database, Conversation conversation)
        {
            return database.DeleteAsync(conversation);
        }
        public static Task<int> DeleteMessageAsync(this SQLiteAsyncConnection database, Message message)
        {
            return database.DeleteAsync(message);
        }
    }
}
