﻿using GreaserFull.Models;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace GreaserFull.Extensions
{
    public static class HubConnectionExtensions
    {

        public static void ReceiveMessageFromHub(this ObservableCollection<Message> conversation, Message incomingMessage)
        {
            conversation.Add(incomingMessage);
        }

        public static void ReceiveConversationFromHub(this ObservableCollection<Message> conversation, Conversation incomingConversation)
        {
            conversation = new ObservableCollection<Message>(incomingConversation.Messages);
        }

        public static void LoadConversationFromHub(this HubConnection hubConnection, int conversationId, ref ObservableCollection<Message> conversation)
        {
            hubConnection.InvokeAsync("LoadConversation", conversationId);
        }

        public static void SendChatMessage(this HubConnection hubConnection, Message userMessage)
        {

        }
    }
}
