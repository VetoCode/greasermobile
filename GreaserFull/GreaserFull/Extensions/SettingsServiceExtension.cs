﻿using GreaserFull.Services;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace GreaserFull.Extensions
{
    public static class SettingsServiceExtension
    {
        public static void DeleteAuthTokenSettings(this ISettingsService settingsService)
        {
            settingsService.AuthAccessToken = null;
            settingsService.AuthRefreshToken = null;
            settingsService.AuthIdentityToken = null;
            settingsService.AuthRefreshToken = null;
        }

        public static void SetAuthTokenSettings(this ISettingsService settingsService, TokenResponse tokenResponse)
        {
            settingsService.AuthAccessToken = tokenResponse.AccessToken;
            settingsService.AuthRefreshToken = tokenResponse.RefreshToken;
            settingsService.AuthIdentityToken = tokenResponse.IdentityToken;
            settingsService.AuthRefreshToken = tokenResponse.RefreshToken;
            settingsService.AuthTokenExpiration = tokenResponse.ExpiresIn.ToString();
        }

        public static void DeleteUserSettings(this ISettingsService settingsService)
        {
            settingsService.Username = string.Empty;
            settingsService.DisplayName = string.Empty;
        }

        public static void SetUserSettings(this ISettingsService settingsService, string claimsContent)
        {
            var claimsArray = JArray.Parse(claimsContent);
            settingsService.Username = claimsArray.SelectToken(JsonPaths.Username).ToString();
            settingsService.DisplayName = claimsArray.SelectToken(JsonPaths.DisplayName).ToString();
        }
    }
}
