﻿using GreaserFull.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace GreaserFull.Extensions
{
    public static class MessageExtensions
    {
        public static Message CreateMessageForHub(this Message message, string userName, string userMessage)
        {
            return message;
        }

        public static Message LastMessage(this ObservableCollection<Message> messageCollection)
        {
            var count = messageCollection.Count;
            return messageCollection[count - 1];
        }
    }
}
