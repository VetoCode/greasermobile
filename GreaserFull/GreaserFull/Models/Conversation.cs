﻿using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace GreaserFull.Models
{
    public class Conversation
    {
        [PrimaryKey]
        public int Id { get; set; }
        [ForeignKey(typeof(Vehicle))]
        public int VehicleId { get; set; }
        public int RequestingUserId { get; set; }
        public int AssignedUserId { get; set; }
        public DateTime LastMessageTimeStamp { get; set; }
        public int LastMessageId { get; set; }
        public bool IsCompleted { get; set; }
        public decimal QuotePrice { get; set; }
        [OneToMany]
        public List<Message> Messages { get; set; }
    }
}
