﻿using GreaserFull.Models;
using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace GreaserFull.Models
{
    public class Vehicle
    {
        [PrimaryKey]
        public int Id { get; set; }
        public string VehicleName { get; set; }
        public int VehicleYear { get; set; }
        public string VehicleModel { get; set; }
        public string VehicleMake { get; set; }
        public string VehicleEngine { get; set; }
        [OneToMany]
        public List<Conversation> Conversations { get; set; }
    }
}
