﻿using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace GreaserFull.Models
{
    public class ServiceRequest
    {

        [PrimaryKey]
        public int Id { get; set; }
        public int ServiceRequestId { get; set; }

        [ForeignKey(typeof(Conversation))]
        public int ConversationId { get; set; }
        public DateTime PhoneTimeStamp { get; set; }
        public DateTime ServerTimeStamp { get; set; }
        public int MessageOrder { get; set; }
        public string QuotePrice { get; set; }
        public string SendingUserId { get; set; }
        public string SendingUserName { get; set; }
        public bool Sent { get; set; }
    }
}
