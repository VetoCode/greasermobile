﻿using GreaserFull.HubModels;
using SQLite;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace GreaserFull.Models
{
    public partial class Message
    {
        [PrimaryKey]
        public int Id { get; set; }

        [ForeignKey(typeof(Conversation))]
        public int ConversationId { get; set; }
        public DateTime PhoneTimeStamp { get; set; }
        public DateTime ServerTimeStamp { get; set; }
        public int MessageOrder { get; set; }
        public string MessageText { get; set; }
        public int SendingUserId { get; set; }
        public string SendingUserName { get; set; }
        public string SendingDisplayName { get; set; }
        public bool IsPending { get; set; }
        public bool IsDeleted { get; set; }
    }

    public partial class Message
    {
        /// <summary>
        /// Create new Message to send to server
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="newMessageText"></param>
        /// <returns></returns>
        public static Message CreateMessage(int conversationId, string newMessageText)
        {
            return new Message()
            {
                ConversationId = conversationId,
                MessageText = newMessageText,
                IsPending = true
            };
        }

        /// <summary>
        /// Create new Message object to send to server
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="newMessageText"></param>
        /// <param name="sendingUserName"></param>
        /// <param name="sendingDisplayName"></param>
        /// <param name="lastMessageNumber"></param>
        /// <returns></returns>
        public static Message CreateMessage(int conversationId, string newMessageText, string sendingUserName, string sendingDisplayName, int lastMessageNumber)
        {
            return new Message()
            {
                ConversationId = conversationId,
                MessageText = newMessageText,
                SendingUserName = sendingUserName,
                SendingDisplayName = sendingDisplayName,
                IsPending = true,
                MessageOrder = lastMessageNumber
            };
        }

        /// <summary>
        /// Create new Message object to send to server
        /// </summary>
        /// <param name="conversationId"></param>
        /// <param name="newMessageText"></param>
        /// <param name="sendingUserName"></param>
        /// <param name="sendingDisplayName"></param>
        /// <param name="lastMessageNumber"></param>
        /// <returns></returns>
        public static HubMessage CreateHubMessage(int conversationId, string newMessageText, string sendingUserName, string sendingDisplayName, int lastMessageNumber)
        {
            return new HubMessage()
            {
                ConversationId = conversationId,
                MessageText = newMessageText,
                SendingUserName = sendingUserName,
                SendingDisplayName = sendingDisplayName,
                IsPending = true,
                MessageOrder = lastMessageNumber
            };
        }
    }
}
