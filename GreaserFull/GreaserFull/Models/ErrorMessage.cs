﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace GreaserFull.Models
{
    public class ErrorMessage
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string TextDump { get; set; }
        public string InnerException { get; set; }
    }
}
