﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace GreaserFull.Behaviors
{
    public static class EntryResponsiveBehavior
    {
        public static readonly BindableProperty AttachBehaviorProperty = BindableProperty.CreateAttached(
            propertyName: "AttachBehavior",
            returnType: typeof(bool),
            declaringType: typeof(EntryResponsiveBehavior),
            defaultValue: false,
            propertyChanged: OnAttachBehaviorChanged);

        public static bool GetAttachBehavior(BindableObject view)
        {
            return (bool)view.GetValue(AttachBehaviorProperty);
        }

        public static void SetAttachBehavior(BindableObject view, bool value)
        {
            view.SetValue(AttachBehaviorProperty, value);
        }

        static void OnAttachBehaviorChanged(BindableObject view, object oldValue, object newValue)
        {
            var entry = view as Entry;
            if (entry == null)
            {
                return;
            }

            bool attachBehavior = (bool)newValue;
            if (attachBehavior)
            {
                entry.Focused += OnEntryFocusChanged;
                entry.Unfocused += OnEntryFocusChanged;
            }
            else
            {
                entry.Focused -= OnEntryFocusChanged;
                entry.Unfocused += OnEntryFocusChanged;
            }
        }

        static void OnEntryFocusChanged(object sender, FocusEventArgs args)
        {
            ((Entry)sender).BackgroundColor = args.IsFocused ? Color.Lavender : Color.Yellow;
        }
    }
}
