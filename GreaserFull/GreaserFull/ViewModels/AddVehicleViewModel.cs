﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace GreaserFull.ViewModels
{
    public class AddVehicleViewModel : BaseViewModel
    {

        #region Command Implementations

        public Command AddVehiclesCommand => new Command(AddVehicle);

        #endregion

        #region UtilityFunctions

        public void AddVehicle()
        {

        }

        #endregion
    }
}
