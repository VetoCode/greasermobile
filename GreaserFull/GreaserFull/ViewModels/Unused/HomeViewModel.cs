﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace GreaserFull.ViewModels
{
    public class HomeViewModel : BaseViewModel
    {
        public HomeViewModel()
        {
            Title = "Greaser";

        }

        public ICommand OpenWebCommand { get; }
    }
}
