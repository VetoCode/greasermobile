﻿using GreaserFull.Extensions;
using GreaserFull.HubModels;
using GreaserFull.Models;
using GreaserFull.Services;
using GreaserFull.Services.Chat;
using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Xml.Linq;
using Xamarin.Forms;

namespace GreaserFull.ViewModels
{
    public class QuoteChatViewModel : BaseViewModel
    {
        int conversationId;
        string userMessage;
        IChatService chatService = ServiceLocator.Resolve<IChatService>();
        public Conversation QuoteConversation;
        public ObservableCollection<Message> ConversationMessages { get; set; }
        public Action<Message> OnMessageReceived;

        public QuoteChatViewModel()
        {
            ConversationMessages = new ObservableCollection<Message>();
            MessagingCenter.Unsubscribe<ChatService, Message>(this, MessageKeys.MessageRecieved);
            MessagingCenter.Subscribe<ChatService, Message>(this, MessageKeys.MessageRecieved, (sender, incomingMessage) =>
            {
                if (!ConversationMessages.Contains(incomingMessage))
                {
                    ConversationMessages.Add(incomingMessage);
                    LoadConversation().Wait();
                }
                    
                OnMessageReceived?.Invoke(incomingMessage);
            });
        }

        #region Properties

        public string UserMessage 
        { 
            get => userMessage; 
            set
            {
                SetProperty(ref userMessage, value);
            }
        }
        public int ConversationId
        {
            get => conversationId;
            set
            {
                conversationId = value;
            }
        }
        #endregion

        #region Command Implementations

        public Command SendMessageCommand => new Command(async () => await AddUserMessageToConversation());

        public Command LoadConversationCommand => new Command(async () => await LoadConversation());

        public Command AcceptQuoteCommand;

        public Command RefreshCommand;

        #endregion

        #region UtilityFunctions

        public override Task InitializeAsync(object conversationId)
        {
            ConversationId = (int)conversationId;
            LoadConversation().Wait();
            return Task.FromResult(true);
        }

        async Task LoadConversation()
        {
            try
            {
                if(conversationId != 0)
                {
                    ConversationMessages = new ObservableCollection<Message>(await chatService.LoadChat(ConversationId));
                    RaisePropertyChanged(() => ConversationMessages);
                }                    
                
            }
            catch (Exception)
            {
                Debug.WriteLine("Failed to Load Messages");
            }
        }

        public async Task AddUserMessageToConversation() 
        {
            if (!String.IsNullOrEmpty(UserMessage))
            {
                var assembledUserMessage = CreateUserMessage();
                await chatService.SendMessage(assembledUserMessage);
            }
        }

        private HubMessage CreateUserMessage()
        {
            return Message.CreateHubMessage(ConversationId, UserMessage, SettingsService.Username, SettingsService.DisplayName, ConversationMessages.LastMessage().MessageOrder);
        }

        #endregion
    }
}
