﻿using GreaserFull.Models;
using GreaserFull.Services;
using GreaserFull.Services.Chat;
using GreaserFull.Views;
using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace GreaserFull.ViewModels
{
    public class ChatsViewModel : BaseViewModel
    {
        Conversation selectedConversation;
        IChatService chatService = ServiceLocator.Resolve<IChatService>();

        public ObservableCollection<Conversation> Conversations { get; }
        public Command LoadConverationsCommand => new Command(async () => await ExecuteLoadConversationsCommand());
        public Command ItemTapped => new Command<Conversation>(OnItemSelected);
        
        public ChatsViewModel()
        {
            Title = "Chats";
            Conversations = new ObservableCollection<Conversation>();

        }

        async Task ExecuteLoadConversationsCommand()
        {
            IsBusy = true;

            try
            {
                Conversations.Clear();
                var items = await chatService.LoadChatList();
                foreach (var item in items)
                {
                    Conversations.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void OnAppearing()
        {
            IsBusy = true;
            SelectedItem = null;
        }
        
        public Conversation SelectedItem
        {
            get => selectedConversation;
            set
            {
                SetProperty(ref selectedConversation, value);
                OnItemSelected(value);
            }
        }

        async void OnItemSelected(Conversation conversation)
        {
            if (conversation == null)
                return;

            await NavigationService.NavigateToAsync<QuoteChatViewModel>(conversation.Id);
        }
    }
}
