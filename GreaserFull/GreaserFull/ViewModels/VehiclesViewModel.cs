﻿using GreaserFull.Views;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace GreaserFull.ViewModels
{
    public class VehiclesViewModel : BaseViewModel
    {
        public Command AddVehiclesCommand => new Command(async () => await AddVehicle());

        async Task AddVehicle()
        {
            await Shell.Current.GoToAsync($"{nameof(AddVehiclePage)}");
        }
        void Handle_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            if (e.Item == null)
                return;

            //await AddVehicle();
            //Deselect Item
            ((ListView)sender).SelectedItem = null;
        }
    }
}
