﻿using GreaserFull.Views;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using GreaserFull.Services;
using System.Linq.Expressions;
using GreaserFull.Services.Authentication;

namespace GreaserFull.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        string username;
        string password;
        bool isLoggedIn;
        IAuthenticationService authService = ServiceLocator.Resolve<IAuthenticationService>();
        
        public Command LoginCommand => new Command(OnLoginClicked);
        public Command NavigateCommand => new Command(OnNavigateClicked);

        public LoginViewModel()
        {
            isLoggedIn = false;
            Username = Constants.TestUsername;
            Password = Constants.TestPassword;
        }

        private void OnLoginClicked()
        {
            SettingsService.Username = Username;
            RaisePropertyChanged(() => UsernameSavedValue);
            if (!string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(Password))
                LoginUser();
        }

        private void OnNavigateClicked()
        {
            NavigationService.NavigateToAsync<VehiclesViewModel>();
        }

        public string Username
        {
            get => username;
            set
            {
                SetProperty(ref username, value);
            }
        }

        public string Password
        {
            get => password;
            set
            {
                SetProperty(ref password, value);
            }
        }

        public bool IsLoggedIn
        {
            get => isLoggedIn;
            set => SetProperty(ref isLoggedIn, value);
        }

        public string UsernameSavedValue
        {
            get
            {
                return SettingsService.Username;
            }
            set => SettingsService.Username = value;
        }

        private async void LoginUser()
        {
            bool loginSuccessful = await authService.LoginAsync(Username, Password);
            //if(loginSuccessful)
                await NavigationService.NavigateToAsync<ChatsViewModel>();
        }
        private async void LogoutUser()
        {
            await authService.LogoutAsync();
        }
    }
}
