﻿using System;
using System.Collections.Generic;
using GreaserFull.ViewModels;
using GreaserFull.Views;
using Xamarin.Forms;

namespace GreaserFull
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public Action<string> LogNavEvent;
        public AppShell()
        {
            InitializeComponent();
            InitializeRoutes();
        }

        private void InitializeRoutes()
        {
            Routing.RegisterRoute(nameof(VehiclesPage), typeof(VehiclesPage));
            Routing.RegisterRoute(nameof(AddVehiclePage), typeof(AddVehiclePage));
            Routing.RegisterRoute(nameof(LoginPage), typeof(LoginPage));
            Routing.RegisterRoute(nameof(QuoteChatPage), typeof(QuoteChatPage));
            Routing.RegisterRoute(nameof(RequestAMechanicPage), typeof(RequestAMechanicPage));
            Routing.RegisterRoute(nameof(ChatsPage), typeof(ChatsPage));
        }

        private async void OnMenuItemClicked(object sender, EventArgs e)
        {
            await Current.GoToAsync("//LoginPage");
        }

        protected override void OnNavigating(ShellNavigatingEventArgs args)
        {
            string argsString = " Target: " + args.Target.ToString();
            LogNavEvent?.Invoke(argsString);
            base.OnNavigating(args);
        }
    }
}
