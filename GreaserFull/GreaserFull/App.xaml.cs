﻿using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using GreaserFull.Services;
using GreaserFull.Views;
using GreaserFull.Data;
using GreaserFull.Models;
using GreaserFull.Services.Session;
using System.Reflection;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace GreaserFull
{
    public partial class App : Application
    {
        static GreaserFullDatabase database;
        ISettingsService settingsService;
        SessionService sessionService;
        public App()
        {
            InitializeComponent();
            RegisterDependencies();
            InitServices();
        }

        public static GreaserFullDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new GreaserFullDatabase();
                }
                return database;
            }
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        private void RegisterDependencies()
        {

        }

        private async void InitServices()
        {
            settingsService = ServiceLocator.Resolve<ISettingsService>();
            sessionService = ServiceLocator.Resolve<SessionService>();
            var navigationService = ServiceLocator.Resolve<INavigationService>();
            await navigationService.InitializeAsync();
        }
    }
}
