﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GreaserFull.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RootNavigationPage : NavigationPage
    {
        public RootNavigationPage()
        {
            InitializeComponent();
        }

        public RootNavigationPage(Page root) : base(root)
        {
            InitializeComponent();
        }
    }
}