﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GreaserFull.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RequestAMechanicPage : ContentPage
    {
        public RequestAMechanicPage()
        {
            InitializeComponent();
        }

        #region ButtonClickEvents

        void RequestBackyardMechanicClicked(object sender, EventArgs args)
        {
            //await label.RelRotateTo(360, 1000);
        }
        void RequestCertifiedMechanicClicked(object sender, EventArgs args)
        {
            // await label.RelRotateTo(360, 1000);
        }
        void RequestGreaserMechanicClicked(object sender, EventArgs args)
        {
            //await label.RelRotateTo(360, 1000);
        }

        #endregion

        #region UtilityFunctions

        void ConnectToApi()
        {

        }

        #endregion
    }
}