﻿using System.ComponentModel;
using Xamarin.Forms;
using GreaserFull.ViewModels;

namespace GreaserFull.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}