﻿using GreaserFull.Models;
using GreaserFull.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GreaserFull.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class QuoteChatPage : ContentPage
    {
        QuoteChatViewModel _viewModel;
        public QuoteChatPage()
        {
            InitializeComponent();
            BindingContext = _viewModel = new QuoteChatViewModel();

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }
    }
}