﻿using GreaserFull.ViewModels;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace GreaserFull.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChatsPage : ContentPage
    {
        ChatsViewModel _chatsViewModel;
        public ObservableCollection<string> Items { get; set; }

        public ChatsPage()
        {
            InitializeComponent();

            BindingContext = _chatsViewModel = new ChatsViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _chatsViewModel.OnAppearing();
        }
    }
}
