﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace GreaserFull
{
    public static class Constants
    {
        public const string BaseUrlLocalHost = "https://localhost";

        public const string BaseUrlPhoneSim = "https://10.0.2.2";

        public const string BaseUrl = BaseUrlLocalHost;

        public const string ChatApiUrl = BaseUrl + ":6001/chathub";

        public const string OrderApiUrl = BaseUrl + ":6001";

        public const string PaymentApiUrl = BaseUrl + ":6001";

        public const string UserClaimsUrl = BaseUrl + ":6001/identity";

        public const string TokenIntrospectionUrl = BaseUrl + ":6001/connect/introspect";

        public const string DevApiServerUrl = BaseUrl + ":6001";

        public const string DevAuthServer = BaseUrl + ":5001";

        public const string ClientId = "GreaserMobileDevice";

        public const string ClientSecret = "Gimobile";

        public const string TestUsername = "contagious.thoughts@gmail.com";

        public const string TestPassword = "##TrainingDay01";

        public const string ApiScopes = "GreaserApi";

        public const string DatabaseFilename = "GreaserSQLite.db3";

        public const SQLite.SQLiteOpenFlags Flags =
            // open the database in read/write mode
            SQLite.SQLiteOpenFlags.ReadWrite |
            // create the database if it doesn't exist
            SQLite.SQLiteOpenFlags.Create |
            // enable multi-threaded database access
            SQLite.SQLiteOpenFlags.SharedCache;

        public static string DatabasePath
        {
            get
            {
                var basePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                return Path.Combine(basePath, DatabaseFilename);
            }
        }
    }
}
