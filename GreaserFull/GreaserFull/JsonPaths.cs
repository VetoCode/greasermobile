﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GreaserFull
{
    public static class JsonPaths
    {
        public const string Username = "$.[?(@.type=='preferred_username')].value";
        public const string DisplayName = "$.[?(@.type=='http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name')].value";
        public const string AuthenticationTime = "$.[?(@.type=='auth_time')].value";
        public const string ExpirationTime = "$.[?(@.type=='exp')].value";
        public const string IssuedAtTime = "$.[?(@.type=='iat')].value";
    }
}
