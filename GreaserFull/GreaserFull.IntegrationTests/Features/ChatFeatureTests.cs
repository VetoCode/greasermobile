﻿using GreaserFull.Extensions;
using GreaserFull.IntegrationTests.Mocks;
using GreaserFull.Services;
using GreaserFull.Services.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;
using Xamarin.Forms.Mocks;
using GreaserFull.ViewModels;
using Xamarin.Forms;
using GreaserFull.Views;
using GreaserFull.Services.Chat;
using Microsoft.AspNetCore.SignalR.Client;
using System.Threading;
using GreaserFull.Models;
using GreaserFull.Services.Settings;

namespace GreaserFull.IntegrationTests.Features
{
    public class ChatFeatureTests : IDisposable
    {
        readonly ITestOutputHelper output;
        ISettingsService settingsService;
        IAuthenticationService authService;
        INavigationService navService;
        IChatService chatService;
        Application application;

        public ChatFeatureTests(ITestOutputHelper outputHelper)
        {
            output = outputHelper;
            MockForms.Init();
            application = Application.Current = new MockApp();

            ServiceLocator.UpdateDependenciesForIntegrationTests(true);
            settingsService = ServiceLocator.Resolve<ISettingsService>();
            authService = ServiceLocator.Resolve<IAuthenticationService>();
            navService = ServiceLocator.Resolve<INavigationService>();
            chatService = ServiceLocator.Resolve<IChatService>();

        }

        public void Dispose()
        {
            settingsService = null;
            authService = null;
        }

        [Fact]
        public void Chatservice_connects_to_api()
        {
            string connectionState = HubConnectionState.Disconnected.ToString();

            if (authService.LoginAsync(Constants.TestUsername, Constants.TestPassword).Result)
            {
                navService.InitializeAsync();
                navService.NavigateToAsync<QuoteChatViewModel>(1);

                var navRootPage = Application.Current.MainPage as RootNavigationPage;
                Type pageTypeReslt = navRootPage.CurrentPage.GetType();
                if (typeof(QuoteChatPage) == pageTypeReslt)
                {
                    connectionState = ((ChatService)chatService).ConnectionState().ToString();
                }
            }
            else
            {
                Assert.True(false, "Login failed. Failing test. Make sure identity server is running");
            }
            output.WriteLine("Connection state: " + connectionState);
            Assert.Equal("Connected", connectionState);
        }

        [Fact]
        public async void Chatservice_sends_and_receives_api_messages()
        {
            string testText = "Test message from ChatFeatureTest: Chatservice_connects_to_api";
            string responseText = string.Empty;

            if (authService.LoginAsync(Constants.TestUsername, Constants.TestPassword).GetAwaiter().GetResult())
            {
                navService.InitializeAsync().Wait();
                navService.NavigateToAsync<QuoteChatViewModel>(1).Wait();

                var navRootPage = Application.Current.MainPage as RootNavigationPage;
                Type pageTypeReslt = navRootPage.CurrentPage.GetType();
                if (typeof(QuoteChatPage) == pageTypeReslt)
                {
                    var quoteChatViewModel = navRootPage.CurrentPage.BindingContext as QuoteChatViewModel;
                    quoteChatViewModel.OnMessageReceived += (Message receivedMessage) =>
                    {
                        output.WriteLine("Message Received!");
                        output.WriteLine("Received message order number: " + receivedMessage.MessageOrder);
                        output.WriteLine("Received message text: " + receivedMessage.MessageText + "\n\n");

                        var messageToBeTested = quoteChatViewModel.ConversationMessages.LastMessage();

                        output.WriteLine("Last message number after MessagingCenter event: " + messageToBeTested.MessageOrder);
                        output.WriteLine("Last message text after MessagingCenter event: " + messageToBeTested.MessageText + "\n\n");
                        
                        responseText = messageToBeTested.MessageText;
                        Assert.Equal(testText, responseText);
                    };

                    var lastMessage = quoteChatViewModel.ConversationMessages.LastMessage();
                    output.WriteLine("Last message number before send message: " + lastMessage.MessageOrder);
                    output.WriteLine("Last message text before send message: " + lastMessage.MessageText + "\n\n");

                    var connectionState = ((ChatService)chatService).ConnectionState().ToString();
                    output.WriteLine("Connection state: " + connectionState + "\n\n");

                    quoteChatViewModel.UserMessage = testText;
                    await quoteChatViewModel.AddUserMessageToConversation();
                }
            }
            else
            {
                Assert.True(false, "Login failed. Failing test. Make sure identity server is running");
            }
        }

        [Fact]
        public void Chatservice_receives_api_messages()
        {
            string testText = "Test message from ChatFeatureTest: Chatservice_connects_to_api";
            string responseText = string.Empty;

            if (authService.LoginAsync(Constants.TestUsername, Constants.TestPassword).Result)
            {
                navService.InitializeAsync();
                navService.NavigateToAsync<QuoteChatViewModel>(1);

                var navRootPage = Application.Current.MainPage as RootNavigationPage;
                Type pageTypeReslt = navRootPage.CurrentPage.GetType();
                if (typeof(QuoteChatPage) == pageTypeReslt)
                {
                    var quoteChatViewModel = navRootPage.CurrentPage.BindingContext as QuoteChatViewModel;
                    quoteChatViewModel.OnMessageReceived += (Message receivedMessage) =>
                    {
                        output.WriteLine("Message Received!");
                        output.WriteLine("Received message order number: " + receivedMessage.MessageOrder);
                        output.WriteLine("Received message text: " + receivedMessage.MessageText + "\n\n");
                        responseText = receivedMessage.MessageText;
                        Assert.Equal(testText, responseText);


                        var lastMessage = quoteChatViewModel.ConversationMessages.LastMessage();
                        Assert.Equal(receivedMessage, lastMessage);
                    };

                    Message testMessage = new Message
                    {
                        ConversationId = 1,
                        Id = 7,
                        MessageOrder = 7,
                        SendingUserId = 101,
                        IsDeleted = false,
                        MessageText = testText,
                        PhoneTimeStamp = DateTime.Now,
                        SendingUserName = "Veto",
                        ServerTimeStamp = DateTime.UtcNow
                    };

                    output.WriteLine("Last message number before send message: " + testMessage.MessageOrder);
                    output.WriteLine("Last message text before send message: " + testMessage.MessageText + "\n\n");
                    MessagingCenter.Send(chatService, MessageKeys.MessageRecieved, testMessage);
                }
            }
            else
            {
                Assert.True(false, "Login failed. Failing test. Make sure identity server is running");
            }
        }
    }
}
