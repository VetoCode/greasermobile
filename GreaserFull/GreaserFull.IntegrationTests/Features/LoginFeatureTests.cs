﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;
using Xamarin.Forms;
using Xamarin.Forms.Mocks;
using GreaserFull.Services;
using GreaserFull.Services.Settings;
using GreaserFull.Services.Authentication;
using System.Threading;
using Newtonsoft.Json.Linq;
using IdentityModel.Client;
using System.Net.Http;

namespace GreaserFull.IntegrationTests.Features
{
    public class LoginFeatureTests : IDisposable
    {
        readonly ITestOutputHelper output;
        readonly ISettingsService settingsService;
        readonly IAuthenticationService authService;

        public LoginFeatureTests(ITestOutputHelper outputHelper)
        {
            output = outputHelper;
            ServiceLocator.UpdateDependenciesForIntegrationTests(true);
            authService = ServiceLocator.Resolve<IAuthenticationService>();
            settingsService = ServiceLocator.Resolve<ISettingsService>();
        }

        public void Dispose()
        {
            
        }

        [Fact]
        public void Login_sends_authentication_request()
        {
            bool success = authService.LoginAsync(Constants.TestUsername, Constants.TestPassword).Result;
            output.WriteLine("Auth access token received: " + settingsService.AuthAccessToken);
            Assert.True(success);
        }

        [Fact]
        public void Bearer_authentication_works()
        {
            bool isLoginSuccessful = authService.LoginAsync(Constants.TestUsername, Constants.TestPassword).Result;
            if (isLoginSuccessful)
            {
                output.WriteLine("Auth access token: " + settingsService.AuthAccessToken);
                output.WriteLine("Auth token expiration time: " + settingsService.AuthTokenExpiration);
                bool isRetrieveUserClaimsSuccessful = authService.RetrieveUserClaims();
                if (isRetrieveUserClaimsSuccessful)
                {
                    output.WriteLine("Username: " + settingsService.Username);
                    output.WriteLine("Display name: " + settingsService.DisplayName);
                }
                else
                {
                    output.WriteLine("Unable to retrive user claims from api server");
                }
                Assert.True(isRetrieveUserClaimsSuccessful);
            }
            else
            {
                Assert.True(isLoginSuccessful, "Unable to login successfully");
            }

        }

        [Fact]
        public void Get_discovery_token_works()
        {
            var discoDocument = authService.GetDiscoveryTokenFromAuthServer().Result;
            output.WriteLine(discoDocument.Json.ToString());
            Assert.False(discoDocument.IsError);

        }

        [Fact]
        public void Bearer_authentication_claims_list()
        {
            authService.LoginAsync(Constants.TestUsername, Constants.TestPassword);
            output.WriteLine("Auth access token: " + settingsService.AuthAccessToken);
            output.WriteLine("Auth token expiration time: " + settingsService.AuthTokenExpiration);

            var apiClient = new HttpClient();
            apiClient.SetBearerToken(settingsService.AuthAccessToken);
            var response2 = apiClient.GetAsync(Constants.UserClaimsUrl).Result;
            if (response2.IsSuccessStatusCode)
            {
                output.WriteLine(response2.StatusCode.ToString());
                var content2 = response2.Content.ReadAsStringAsync().Result;
                output.WriteLine(JArray.Parse(content2).ToString());
            }
            else
            {
                output.WriteLine("Unable to retrive user claims from api server");
            }
        }

        [Fact(Skip = "Long running")]
        public void Token_expiration_counts_in_seconds()
        {
            authService.LoginAsync(Constants.TestUsername, Constants.TestPassword);

            var apiClient = new HttpClient();
            apiClient.SetBearerToken(settingsService.AuthAccessToken);
            TokenIntrospectionResponse tokenIntrospectionResponse;
            HttpResponseMessage response;
            string authenticationTime, expirationTime, issuedAtTime, content;
            JArray claimsArray;
            for (int i = 0; i < 0; i++)
            {
                response = apiClient.GetAsync(Constants.UserClaimsUrl).Result;
                output.WriteLine("Auth access token: " + settingsService.AuthAccessToken);
                tokenIntrospectionResponse = authService.IntrospectAccessToken();
                if (response.IsSuccessStatusCode)
                {
                    output.WriteLine("IntrospectiveToken error: " + tokenIntrospectionResponse.Error);
                    output.WriteLine("IntrospectiveToken error type: " + tokenIntrospectionResponse.ErrorType);
                    //output.WriteLine("IntrospectiveToken is active: " + tokenIntrospectionResponse.IsActive);
                    output.WriteLine("IntrospectiveToken dump: " + tokenIntrospectionResponse.Raw);

                    output.WriteLine("Response status code: " + response.StatusCode.ToString());
                    output.WriteLine("Response headers: " + response.Headers.ToString());

                    content = response.Content.ReadAsStringAsync().Result;
                    claimsArray = JArray.Parse(content);

                    authenticationTime = DateTimeOffset.FromUnixTimeSeconds(int.Parse(claimsArray.SelectToken(JsonPaths.AuthenticationTime).ToString())).ToString();
                    expirationTime = DateTimeOffset.FromUnixTimeSeconds(int.Parse(claimsArray.SelectToken(JsonPaths.ExpirationTime).ToString())).ToString();
                    issuedAtTime = DateTimeOffset.FromUnixTimeSeconds(int.Parse(claimsArray.SelectToken(JsonPaths.IssuedAtTime).ToString())).ToString();

                    output.WriteLine("Current time: " + DateTime.UtcNow.ToString());
                    output.WriteLine("Authentication time: " + authenticationTime);
                    output.WriteLine("Expiration time: " + expirationTime);
                    output.WriteLine("Issued at time: " + issuedAtTime + "\n \n");
                }
                else
                {
                    output.WriteLine("Response dump: " + response.Content.ReadAsStringAsync().Result);
                    output.WriteLine("Unable to retrive user claims from api server");
                }
                Thread.Sleep(15000);
            }

        }

        [Fact(Skip = "Introspection endpoint doesn't work")]
        public void Token_Introspection_works()
        {
            if (!authService.LoginAsync(Constants.TestUsername, Constants.TestPassword).Result)
            {
                Assert.True(false, "Unable to login");
            }
            var tokenIntrospectionResponse = authService.IntrospectAccessToken();
            if (tokenIntrospectionResponse.IsError)
            {
                output.WriteLine("Error reaching introspection endpoint");
                output.WriteLine("Error: " + tokenIntrospectionResponse.Error);
                output.WriteLine("Error Raw: " + tokenIntrospectionResponse.Raw);
                Assert.True(false);
            }
            else
            {
                output.WriteLine("IntrospectionToken raw content: " + tokenIntrospectionResponse.Raw);
                output.WriteLine("IntrospectionToken is active: " + tokenIntrospectionResponse.IsActive);
                output.WriteLine("IntrospectionToken dump: " + tokenIntrospectionResponse.Claims.ToString());
                Assert.False(tokenIntrospectionResponse.IsError);
            }
        }
    }
}
