using GreaserFull.Test.Mocks;
using GreaserFull.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Mocks;
using Xunit;
using Xunit.Abstractions;

namespace GreaserFull.Test;

public class MockXamarinEnvironmentTests : IDisposable
{
    private readonly ITestOutputHelper output;
    private Application application;

    public MockXamarinEnvironmentTests(ITestOutputHelper outputHelper)
    {
        output = outputHelper;
        MockForms.Init();
        application = Application.Current = new MockApp();
    }
    public void Dispose()
    {
        Application.Current = null;
    }

    [Fact]
    public void Mock_forms_environment_created()
    {
        Assert.NotNull(application);
    }

    [Fact]
    public void Mock_forms_mainpage_accesssable()
    {
        application.MainPage = new RootNavigationPage(new LoginPage());
        var rootNavMainPage = application.MainPage as RootNavigationPage;
        Assert.NotNull(rootNavMainPage);
    }
}