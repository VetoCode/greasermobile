﻿using GreaserFull.Services;
using GreaserFull.Services.Authentication;
using GreaserFull.Test.Mocks;
using GreaserFull.ViewModels;
using GreaserFull.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Mocks;
using Xunit;
using Xunit.Abstractions;

namespace GreaserFull.Test.Services.Navigation
{
    public class NavigationServiceTests : IDisposable
    {
        readonly ITestOutputHelper output;
        readonly IAuthenticationService authenticationService;
        readonly Application application;

        public NavigationServiceTests(ITestOutputHelper outputHelper)
        {
            output = outputHelper;
            MockForms.Init();
            ServiceLocator.Register<IAuthenticationService, MockAuthenticationService>();
            authenticationService = ServiceLocator.Resolve<IAuthenticationService>();
            application = Application.Current = new MockApp();
        }

        public void Dispose()
        {
            Application.Current = null;
        }

        [Fact]
        public void Unauthenticated_Users_Start_On_Login_Page()
        {

            authenticationService.IsAuthenticated = false;
            NavigationService navigationService = ServiceLocator.Resolve<NavigationService>();
            navigationService.InitializeAsync();

            Type pageTypeResult;
            if (Application.Current.MainPage is RootNavigationPage navRootPage)
            {
                pageTypeResult = navRootPage.RootPage.GetType();
                output.WriteLine("Page result type: " + pageTypeResult.FullName);
                Assert.Equal(typeof(LoginPage), pageTypeResult);
            } 
            else
            {
                output.WriteLine("Nav root page is null");
                Assert.True(false);
            }
        }

        [Fact]
        public async void Login_Page_Is_Removed_From_Navigation_Stack_AfterLogin()
        {
            authenticationService.IsAuthenticated = false;
            NavigationService navigationService = ServiceLocator.Resolve<NavigationService>();
            authenticationService.IsAuthenticated = true;
            await navigationService.NavigateToAsync<ChatsViewModel>();
            var navRootPage = Application.Current.MainPage as RootNavigationPage;
            Type pageTypeResult;
            try
            {
                pageTypeResult = navRootPage.CurrentPage.GetType();

            }
            catch (NullReferenceException nullException)
            {
                output.WriteLine(nullException.ToString());
                Assert.True(false, "Received a null reference exception when accessing Application.Current.MainPage");
            }
            Assert.True(true);
        }

        [Fact]
        public void Authenticated_Users_Start_On_Home_Page()
        {

            authenticationService.IsAuthenticated = true;
            NavigationService navigationService = ServiceLocator.Resolve<NavigationService>();
            navigationService.InitializeAsync();

            var navRootPage = Application.Current.MainPage as RootNavigationPage;

            Type pageTypeResult;
            try
            {
                pageTypeResult = navRootPage.CurrentPage.GetType();
                output.WriteLine("Page result type: " + pageTypeResult.FullName);
                Assert.Equal(typeof(HomePage), pageTypeResult);
            }
            catch (NullReferenceException nullException)
            {
                output.WriteLine(nullException.ToString());
                Assert.True(false);
            }
        }
    }
}
