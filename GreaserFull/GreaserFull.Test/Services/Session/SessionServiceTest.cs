﻿using GreaserFull.Services;
using GreaserFull.Services.Authentication;
using GreaserFull.Services.Session;
using GreaserFull.Test.Mocks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Mocks;
using Xunit;
using Xunit.Abstractions;

namespace GreaserFull.Test.Services.Session
{
    public class SessionServiceTest : IDisposable
    {
        readonly ITestOutputHelper output;
        readonly Application application;
        readonly IAuthenticationService authenticationService;
        readonly SessionService sessionService;

        public SessionServiceTest(ITestOutputHelper outputHelper)
        {
            this.output = outputHelper;
            InitializeMockServices();
            InitializeMockEnvironment();
        }

        void InitializeMockServices()
        {
            ServiceLocator.Register<IAuthenticationService, MockAuthenticationService>();
            authenticationService = ServiceLocator.Resolve<IAuthenticationService>();
            sessionService = ServiceLocator.Resolve<SessionService>();
        }

        void InitializeMockEnvironment()
        {
            MockForms.Init();
            application = Application.Current = new MockApp();
        }

        public void Dispose()
        {
            Application.Current = null;
        }

        [Fact]
        public void Session_receives_login_message()
        {
            int sessionDurationInSeconds = ((int)sessionService.SessionDuration.TotalSeconds);

            Assert.Equal(0, sessionDurationInSeconds);
            
            MessagingCenter.Send(authenticationService, MessageKeys.Login, 5);
            
            sessionDurationInSeconds = ((int)sessionService.SessionDuration.TotalSeconds);
            Assert.Equal(5, sessionDurationInSeconds);

            MessagingCenter.Send(authenticationService, MessageKeys.Login, 0);
            sessionDurationInSeconds = ((int)sessionService.SessionDuration.TotalSeconds);
            Assert.Equal(0, sessionDurationInSeconds);
        }

        [Fact]
        public void Session_starts_when_valid_login_message_received()
        {
            sessionService.OnSessionStarted += () =>
            {
                output.WriteLine("Start session called.");
                output.WriteLine("DateTime.Now is earlier than expiration time check.");
                output.WriteLine("DateTime now: " + DateTime.Now);
                output.WriteLine("Session expiration time: " + sessionService.SessionExpirationTime + "\n");
                Assert.True(DateTime.Now < sessionService.SessionExpirationTime);
            };

            MessagingCenter.Send(authenticationService, MessageKeys.Login, 0);

            MessagingCenter.Send(authenticationService, MessageKeys.Login, 5);

            MessagingCenter.Send(authenticationService, MessageKeys.Login, 0);
        }

        [Fact]
        public void Session_receives_logout_message()
        {
            bool logoutCalled = false;

            sessionService.OnSessionExpired += (LogoutReason reason) =>
            {
                output.WriteLine("Logout was called.");
                output.WriteLine("Logout reason: " + reason);
                logoutCalled = true;
            };

            Assert.False(logoutCalled);

            MessagingCenter.Send(authenticationService, MessageKeys.Logout, LogoutReason.User_initiated);

            Assert.True(logoutCalled);
        }

        [Fact]
        public void Session_ends_after_duration()
        {
            sessionService.OnSessionExpired += (LogoutReason reason) =>
            {
                output.WriteLine("Logout was called.");
                output.WriteLine("DateTime.Now greater than expiration time check.");
                output.WriteLine("DateTime now: " + DateTime.Now);
                output.WriteLine("Session expiration time: " + sessionService.SessionExpirationTime + "\n");
                Assert.True(DateTime.Now > sessionService.SessionExpirationTime);
            };

            MessagingCenter.Send(authenticationService, MessageKeys.Login, 0);
            
            System.Threading.Thread.Sleep(5000);

            output.WriteLine("DateTime.Now greater than expiration time, 5 seconds check.");
            output.WriteLine("DateTime now: " + DateTime.Now);
            output.WriteLine("Session expiration time: " + sessionService.SessionExpirationTime + "\n");
            Assert.True(DateTime.Now > sessionService.SessionExpirationTime);

            MessagingCenter.Send(authenticationService, MessageKeys.Login, 10);

            output.WriteLine("DateTime.Now less than expiration time check.");
            output.WriteLine("DateTime now: " + DateTime.Now);
            output.WriteLine("Session expiration time: " + sessionService.SessionExpirationTime + "\n");
            Assert.True(DateTime.Now < sessionService.SessionExpirationTime);
        }
    }
}
