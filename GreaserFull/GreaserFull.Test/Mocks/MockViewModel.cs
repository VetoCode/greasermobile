﻿using GreaserFull.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreaserFull.Test.Mocks
{
    public class MockViewModel : BaseViewModel
    {
        public object? lastNavigationDataRecieved { get; set; }
        public override async Task InitializeAsync(object navigationData)
        {
            lastNavigationDataRecieved = navigationData;
        }
    }
}
