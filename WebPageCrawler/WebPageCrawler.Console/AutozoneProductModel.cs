﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebPageCrawler.Crawler
{
    public class AutozoneProductModel
    {
        public string Name { get; set; }
        public string ProductUrl { get; set; }
        public string Sku { get; set; }
        public string PartNumber { get; set; }
        public string ImageUrl { get; set; }
        public string CurrencyPrefix { get; set; }
        public decimal Price { get; set; }
        public List<ItemCategory> Categories { get; set; }
        public string FullBreadCrumb { get; set; }
        public DateTime lastModified { get; set; }

    }
    public class ItemCategory
    {
        public string Name { get; set; }
        public string CategoryUrl { get; set; }
        public string ParentCategoryName { get; set; }
        public string ParentCategoryUrl { get; set; }
    }
}
