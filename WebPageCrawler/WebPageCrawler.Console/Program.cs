﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace WebPageCrawler.Crawler
{
    class Program
    {
        static void Main(string[] args)
        {
            var baseUrl = @"https://www.autozone.com/parts";
            var url2 = @"https://www.autozone.com/ignition-tune-up-and-routine-maintenance/ballast-resistor";
            //HtmlDocument doc = GetURLData(url2);
            // var autozoneProducts = ExtractAutozoneProductDetails(doc);
            CrawlAutozoneWebsite(baseUrl);
        }
        public static void CrawlAutozoneWebsite(string url)
        {
            HtmlDocument doc = GetURLData(url);
            var currentCategoryName = doc.DocumentNode.SelectSingleNode("//nav/ol").LastChild.SelectSingleNode("//span/span").InnerText;
            var categoryNodes = doc.DocumentNode.SelectSingleNode("//div[@class='MuiGrid-root styles__shopByRangeContainer--1YVUB MuiGrid-container MuiGrid-spacing-xs-1']");
            if (categoryNodes != null)
            {
                List<ItemCategory> categories = new List<ItemCategory>();
                var categoryNodesChildren = categoryNodes.ChildNodes;
                foreach (var node in categoryNodesChildren)
                {
                    categories.Add(new ItemCategory()
                    {
                        ParentCategoryUrl = url,
                        ParentCategoryName = currentCategoryName,
                        Name = node.SelectSingleNode("./a/div/h3").InnerText,
                        CategoryUrl = @"https://www.autozone.com" + node.FirstChild.Attributes["href"].Value
                    });
                }
                foreach (var category in categories)
                {
                    CrawlAutozoneWebsite(category);
                }
            } 
            else {
                var autozoneProductList = ExtractAutozoneProductDetails(doc, url);
            }
        }
        public static void CrawlAutozoneWebsite(ItemCategory itemCategory)
        {
            HtmlDocument doc = GetURLData(itemCategory.CategoryUrl);
            var categoryNodes = doc.DocumentNode.SelectSingleNode("//div[@class='MuiGrid-root styles__shopByRangeContainer--1YVUB MuiGrid-container MuiGrid-spacing-xs-1']");
            if (categoryNodes != null)
            {
                List<ItemCategory> categories = new List<ItemCategory>();
                var categoryNodesChildren = categoryNodes.ChildNodes;
                foreach (var node in categoryNodesChildren)
                {
                    categories.Add(new ItemCategory()
                    {
                        ParentCategoryUrl = itemCategory.CategoryUrl,
                        ParentCategoryName = itemCategory.Name,
                        Name = node.SelectSingleNode("./a/div/h3").InnerText,
                        CategoryUrl = @"https://www.autozone.com" + node.FirstChild.Attributes["href"].Value
                    });
                }
                foreach (var category in categories)
                {
                    CrawlAutozoneWebsite(category);
                }
            }
            else
            {
                var autozoneProductList = ExtractAutozoneProductDetails(doc, itemCategory.CategoryUrl);
            }
        }
        public static HtmlDocument GetURLData(string URL)
        {
            HtmlDocument doc = new HtmlDocument();
            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(URL);
                //request.UserAgent = "Omurcek";
                request.Timeout = 30000;
                request.Accept = @"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9";
                request.KeepAlive = true;
                WebResponse response = request.GetResponse();
                Stream stream = response.GetResponseStream();
                StreamReader reader = new StreamReader(stream);
                string data = reader.ReadToEnd();
                doc.LoadHtml(data);
                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Receive DATA Error : " + URL + ex.ToString());
            }
            return doc;
        }
        public static List<AutozoneProductModel> ExtractAutozoneProductDetails(HtmlDocument doc)
        {
            List<AutozoneProductModel> autozoneProducts = new List<AutozoneProductModel>();
            var productNodes = doc.DocumentNode.SelectNodes("//div[@data-testid='product-container']");

            foreach (var node in productNodes)
            {
                string dollars = node.SelectNodes(".//div[@id='priceContainer']/div/div[@class='styles__price--1CieW']").First().InnerText;
                string cents = node.SelectNodes(".//div[@id='priceContainer']/div/div[@class='styles__dec--ynQkD']").First().InnerText;
                decimal itemPrice = decimal.Parse(dollars + "." + cents);
                autozoneProducts.Add(new AutozoneProductModel
                {
                    Name = node.SelectNodes(".//h3[@id='productTitle']").First().InnerText,
                    ProductUrl = node.SelectNodes(".//a[@class='globals-module__az-color-inherit--1yqHz']").First().Attributes["href"].Value,
                    Sku = node.SelectNodes(".//div[@data-testid='product-sku-number']//span[@class='styles__value--_4mIk']").First().InnerText,
                    PartNumber = node.SelectNodes(".//div[@data-testid='product-part-number']//span[@class='styles__value--_4mIk']").First().InnerText,
                    ImageUrl = node.SelectNodes(".//img[@id='productImage']").First().Attributes["src"].Value,
                    CurrencyPrefix = node.SelectNodes(".//div[@id='priceContainer']/div/div[@class='styles__prefix--1sFQv']").First().InnerText,
                    Price = itemPrice,
                    lastModified = DateTime.Today
                });
            }
            
            return autozoneProducts;
        }
        public static List<AutozoneProductModel> ExtractAutozoneProductDetails(HtmlDocument doc, string url)
        {
            List<AutozoneProductModel> autozoneProducts = new List<AutozoneProductModel>();
            bool nextPage = true;
            int nextPageNumber = 2;
            while (nextPage)
            {
                var productNodes = doc.DocumentNode.SelectNodes(".//div[@data-testid='product-container']");
                foreach (var node in productNodes)
                {
                    string dollars = node.SelectNodes(".//div[@id='priceContainer']/div/div[@class='styles__price--1CieW']").First().InnerText;
                    string cents = node.SelectNodes(".//div[@id='priceContainer']/div/div[@class='styles__dec--ynQkD']").First().InnerText;
                    decimal itemPrice = decimal.Parse(dollars + "." + cents);
                    string tName = node.SelectNodes(".//h3[@id='productTitle']").First().InnerText;
                    string tProductUrl = node.SelectNodes(".//a[@class='globals-module__az-color-inherit--1yqHz']").First().Attributes["href"].Value;
                    string tSku = node.SelectNodes(".//div[@data-testid='product-sku-number']//span[@class='styles__value--_4mIk']").First().InnerText;
                    string tPartNumber = node.SelectNodes(".//div[@data-testid='product-part-number']//span[@class='styles__value--_4mIk']").First().InnerText;
                    string tImageUrl = node.SelectNodes(".//img[@id='productImage']").First().Attributes["src"].Value;
                    string tCurrencyPrefix = node.SelectNodes(".//div[@id='priceContainer']/div/div[@class='styles__prefix--1sFQv']").First().InnerText;
                    autozoneProducts.Add(new AutozoneProductModel
                    {
                        Name = tName,
                        ProductUrl = tProductUrl,
                        Sku = tSku,
                        PartNumber = tPartNumber,
                        ImageUrl = tImageUrl,
                        CurrencyPrefix = tCurrencyPrefix,
                        Price = itemPrice,
                        lastModified = DateTime.Today
                    });
                }
                var paginationAttributes = doc.DocumentNode.SelectSingleNode("//div[@id='paginationButton']").LastChild.Attributes;
                foreach (var att in paginationAttributes)
                {
                    if (att.Name == "disabled") { nextPage = false; }
                }
                if (nextPage)
                {
                    string nextPageUrl = url + "?pageNumber=" + nextPageNumber;
                    doc = GetURLData(nextPageUrl);
                    nextPageNumber++;
                }
            };

            return autozoneProducts;
        }
    }
}
